use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let mut board = Board::load();
    let mut count = 1;
    while board.step() > 0 {
        count += 1;
    }
    println!("P1: {}", count);
}

#[derive(Eq, PartialEq, Copy, Clone)]
enum Cell {
    Empty,
    East,
    South,
}

#[derive(Clone, Eq, PartialEq)]
struct Board {
    rows: Vec<Vec<Cell>>
}

impl Board {
    pub fn load() -> Board {
        let reader = BufReader::new(File::open("day25/input.txt").unwrap());
        Board {
            rows: reader.lines().map(|l| l.unwrap()).filter(|l| !l.is_empty()).map(|l| {
                l.chars().map(|c| {
                    match c {
                        '.' => Cell::Empty,
                        '>' => Cell::East,
                        'v' => Cell::South,
                        _ => panic!("Unexpected char: {:?}", c)
                    }
                }).collect()
            }).collect()
        }
    }

    pub fn step(&mut self) -> usize {
        let mut move_count = 0;

        {
            let mut moves = vec![];

            let rows = self.rows.len();
            for r in 0..rows {
                let cols = self.rows[r].len();
                for c in 0..cols {
                    let old = (r, c);
                    let new = (r, (c + 1) % cols);
                    if self.rows[old.0][old.1] == Cell::East && self.rows[new.0][new.1] == Cell::Empty {
                        moves.push((old, new));
                    }
                }
            }

            move_count += moves.len();

            for (old, new) in moves {
                self.rows[new.0][new.1] = self.rows[old.0][old.1];
                self.rows[old.0][old.1] = Cell::Empty;
            }
        }

        {
            let mut moves = vec![];

            let rows = self.rows.len();
            for r in 0..rows {
                let cols = self.rows[r].len();
                for c in 0..cols {
                    let old = (r, c);
                    let new = ((r + 1) % rows, c);
                    if self.rows[old.0][old.1] == Cell::South && self.rows[new.0][new.1] == Cell::Empty {
                        moves.push((old, new));
                    }
                }
            }

            move_count += moves.len();

            for (old, new) in moves {
                self.rows[new.0][new.1] = self.rows[old.0][old.1];
                self.rows[old.0][old.1] = Cell::Empty;
            }
        }

        move_count
    }
}