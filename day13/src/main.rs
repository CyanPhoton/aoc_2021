use std::collections::{HashSet, VecDeque};
use std::fmt::{Debug, Formatter};
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    problem_1();
    problem_2();
}

enum Fold {
    X(u32),
    Y(u32)
}

#[derive(Default)]
struct Sheet {
    dots: HashSet<(u32, u32)>,
    folds: VecDeque<Fold>
}

impl Sheet {
    pub fn do_fold(&mut self) -> bool {
        if self.folds.is_empty() {
            return false;
        }

        match self.folds.pop_front().unwrap() {
            Fold::X(reflect_x) => {
                self.dots = self.dots.drain().map(|(x, y)| (reflect_x - (x as i32 - reflect_x as i32).abs() as u32, y)).collect();
            }
            Fold::Y(reflect_y) => {
                self.dots = self.dots.drain().map(|(x, y)| (x, reflect_y - (y as i32 - reflect_y as i32).abs() as u32)).collect();
            }
        }

        true
    }

    pub fn dot_count(&self) -> usize {
        self.dots.len()
    }
}

impl Debug for Sheet {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let max_x = self.dots.iter().map(|p| p.0).max().unwrap();
        let max_y = self.dots.iter().map(|p| p.1).max().unwrap();

        for y in 0..=max_y {
            for x in 0..=max_x {
                write!(f, "{}", if self.dots.contains(&(x, y)) { "#" } else { "." })?;
            }
            write!(f, "\n");
        }

        Ok(())
    }
}

fn read_input() -> Sheet {
    let reader = BufReader::new(File::open("day13/input.txt").unwrap());
    let mut sheet = Sheet::default();

    for line in reader.lines() {
        let line = line.unwrap();
        if line.is_empty() {
            continue;
        }

        if let Some((dir, pos)) = line.split_once('=') {
            let pos = pos.parse().unwrap();
            if dir.ends_with('x') {
                sheet.folds.push_back(Fold::X(pos));
            } else {
                sheet.folds.push_back(Fold::Y(pos));
            }
        } else {
            let (x, y) = line.split_once(',').unwrap();
            let pos = (x.parse::<u32>().unwrap(), y.parse::<u32>().unwrap());
            sheet.dots.insert(pos);
        }
    }

    sheet
}

fn problem_1() {
    let mut sheet = read_input();

    sheet.do_fold();

    println!("P1: {}", sheet.dot_count());
}

fn problem_2() {
    let mut sheet = read_input();

    while sheet.do_fold() {}

    println!("P2: {}\n{:?}", sheet.dot_count(), sheet);
}
