use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    problem_1();
    problem_2();
}

fn problem_1() {
    let reader = BufReader::new(File::open("day10/input.txt").unwrap());

    let closing_map = {
        let mut closing_map = HashMap::new();
        closing_map.insert('(', ')');
        closing_map.insert('[', ']');
        closing_map.insert('{', '}');
        closing_map.insert('<', '>');
        closing_map
    };

    let error_score_map = {
        let mut error_score_map = HashMap::new();
        error_score_map.insert(')', 3);
        error_score_map.insert(']', 57);
        error_score_map.insert('}', 1197);
        error_score_map.insert('>', 25137);
        error_score_map
    };

    let mut error_score = 0;

    for line in reader.lines() {
        let line = line.unwrap();
        let mut scope_stack = Vec::new();

        for c in line.chars() {
            match c {
                '(' | '[' | '{' | '<' => scope_stack.push(c),
                ')' | ']' | '}' | '>' => {
                    let opening = scope_stack.pop().unwrap();
                    let expected_closing = closing_map[&opening];

                    if c != expected_closing {
                        println!("Corrupt line: {:?}", line);
                        error_score += error_score_map[&c];
                        break;
                    }
                }
                _ => panic!("Unexpected char: {}", c)
            }
        }
    }

    println!("P1 error score: {}", error_score);
}

fn problem_2() {
    let reader = BufReader::new(File::open("day10/input.txt").unwrap());

    let closing_map = {
        let mut closing_map = HashMap::new();
        closing_map.insert('(', ')');
        closing_map.insert('[', ']');
        closing_map.insert('{', '}');
        closing_map.insert('<', '>');
        closing_map
    };

    let completion_score_map = {
        let mut completion_score_map = HashMap::new();
        completion_score_map.insert(')', 1);
        completion_score_map.insert(']', 2);
        completion_score_map.insert('}', 3);
        completion_score_map.insert('>', 4);
        completion_score_map
    };

    let mut completion_scores = Vec::new();

    'lines: for line in reader.lines() {
        let line = line.unwrap();
        let mut scope_stack = Vec::new();

        for c in line.chars() {
            match c {
                '(' | '[' | '{' | '<' => scope_stack.push(c),
                ')' | ']' | '}' | '>' => {
                    let opening = scope_stack.pop().unwrap();
                    let expected_closing = closing_map[&opening];

                    if c != expected_closing {
                        println!("Corrupt line (skipping): {:?}", line);
                        continue 'lines;
                    }
                }
                _ => panic!("Unexpected char: {}", c)
            }
        }

        if !scope_stack.is_empty() {
            let mut completion_score: i128 = 0;

            while let Some(next) = scope_stack.pop() {
                completion_score *= 5;
                let closing = closing_map[&next];
                completion_score += completion_score_map[&closing];
            }

            completion_scores.push(completion_score);
        }
    }

    completion_scores.sort();
    let completion_score = completion_scores[completion_scores.len() / 2];

    dbg!(&completion_scores);

    println!("P2 completion score: {}", completion_score);
}
