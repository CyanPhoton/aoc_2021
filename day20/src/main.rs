use std::fmt::{Debug, Formatter};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::{Index, IndexMut};
use common::CollectN;

fn main() {
    let (kernel, image) = load_data();

    {
        let mut inter = image.clone();
        for _ in 0..2 {
            inter = kernel.process_image(&inter);
        }

        println!("P1: {}", inter.count_ones());
    }

    {
        let mut inter = image.clone();
        for _ in 0..50 {
            inter = kernel.process_image(&inter);
        }

        println!("P2: {}", inter.count_ones());
    }
}

pub struct KernelMap {
    pub map: [u8; 512]
}

#[derive(Clone)]
pub struct ImageView {
    image_width: usize,
    image_height: usize,
    image: Vec<u8>,
    outside_value: u8
}

pub fn load_data() -> (KernelMap, ImageView) {
    let reader = BufReader::new(File::open("day20/input.txt").unwrap());
    let mut lines = reader.lines();

    let map = lines.next().unwrap().unwrap().chars().map(|c| if c == '#' { 1 } else { 0 }).collect_n().unwrap();

    let lines = lines.map(|l| l.unwrap()).filter(|l| !l.is_empty()).collect::<Vec<_>>();
    let image_height = lines.len();

    let image = lines.iter().flat_map(|l| {
        l.chars().map(|c| if c == '#' { 1 } else { 0 })
    }).collect::<Vec<_>>();

    let image_width = image.len() / image_height;

    (
        KernelMap { map },
        ImageView { image_width, image_height, image, outside_value: 0 }
    )
}

impl ImageView {
    pub fn new_zero(width: usize, height: usize) -> ImageView {
        ImageView {
            image_width: width,
            image_height: height,
            image: vec![0; width * height],
            outside_value: 0
        }
    }

    fn coord_to_index(&self, [x, y]: [usize; 2]) -> usize {
        self.image_width * y + x
    }

    // In L->R, T->B
    // radius = n -> (2n - 1)x(2n - 1)
    // Outside image, returns 0
    pub fn for_kernel<F: FnMut(u8)>(&self, [cx, cy]: [isize; 2], radius: usize, mut f: F) {
        for y in (cy - radius as isize + 1)..=(cy + radius as isize - 1) {
            for x in (cx - radius as isize + 1)..=(cx + radius as isize - 1) {
                if x < 0 || y < 0 || x as usize >= self.image_width || y as usize >= self.image_height {
                    f(self.outside_value);
                } else {
                    f(self[[x as usize, y as usize]]);
                }
            }
        }
    }

    pub fn len(&self) -> [usize; 2] {
        [self.image_width, self.image_height]
    }

    pub fn count_ones(&self) -> usize {
        self.image.iter().filter(|n| **n == 1).count()
    }
}

impl KernelMap {
    pub fn process_image(&self, image: &ImageView) -> ImageView {
        let mut result = ImageView::new_zero(image.image_width + 2, image.image_height + 2);

        for y in 0..result.image_height {
            for x in 0..result.image_width {
                let mut index = 0;
                image.for_kernel([x as isize - 1, y as isize - 1], 2, |n| {
                    index <<= 1;
                    index |= n as usize;
                });

                result[[x, y]] = self.map[index];
            }
        }

        if image.outside_value == 0 {
            result.outside_value = self.map[0];
        } else {
            result.outside_value = *self.map.last().unwrap();
        }

        result
    }
}

impl Debug for ImageView {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "ImageView:")?;

        for _ in 0..self.image_width+2 {
            write!(f, "{} ", if self.outside_value == 1 { '#' } else { '.' })?;
        }
        write!(f, "\n")?;

        for y in 0..self.image_height {
            write!(f, "{} ", if self.outside_value == 1 { '#' } else { '.' })?;
            for x in 0..self.image_width {
                write!(f, "{} ", if self[[x, y]] == 1 { '#' } else { '.' })?;
            }
            write!(f, "{} ", if self.outside_value == 1 { '#' } else { '.' })?;
            write!(f, "\n")?;
        }

        for _ in 0..self.image_width+2 {
            write!(f, "{} ", if self.outside_value == 1 { '#' } else { '.' })?;
        }
        write!(f, "\n")?;

        Ok(())
    }
}

impl Index<[usize; 2]> for ImageView {
    type Output = u8;

    fn index(&self, index: [usize; 2]) -> &u8 {
        let index = self.coord_to_index(index);
        self.image.index(index)
    }
}

impl IndexMut<[usize; 2]> for ImageView {
    fn index_mut(&mut self, index: [usize; 2]) -> &mut u8 {
        let index = self.coord_to_index(index);
        self.image.index_mut(index)
    }
}