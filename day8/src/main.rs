use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::{BitAnd, BitOr, BitXor, Not, Sub};
use crate::Segment::*;

fn main() {
    problem_1();
    problem_2();
}

#[repr(u8)]
enum Segment {
    A = 1 << 0,
    B = 1 << 1,
    C = 1 << 2,
    D = 1 << 3,
    E = 1 << 4,
    F = 1 << 5,
    G = 1 << 6
}

impl BitOr<Segment> for Segment {
    type Output = SevenSegment;

    fn bitor(self, rhs: Segment) -> Self::Output {
        SevenSegment { segments: self as u8 | rhs as u8 }
    }
}

impl BitOr<Segment> for SevenSegment {
    type Output = SevenSegment;

    fn bitor(self, rhs: Segment) -> Self::Output {
        SevenSegment { segments: self.segments | rhs as u8 }
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct SevenSegment {
    pub segments: u8
}

impl SevenSegment {
    pub fn new(s: &str) -> Self {
        let mut segments = 0;

        for c in s.chars() {
            segments |= (1u8 << (c as u8 - 'a' as u8) as u32)
        }

        SevenSegment {
            segments
        }
    }

    pub fn len(self) -> usize {
        self.segments.count_ones() as usize
    }
}

impl BitOr for SevenSegment {
    type Output = SevenSegment;

    fn bitor(self, rhs: Self) -> Self::Output {
        SevenSegment { segments: self.segments | rhs.segments }
    }
}

impl BitAnd for SevenSegment {
    type Output = SevenSegment;

    fn bitand(self, rhs: Self) -> Self::Output {
        SevenSegment { segments: self.segments & rhs.segments }
    }
}

impl BitXor for SevenSegment {
    type Output = SevenSegment;

    fn bitxor(self, rhs: Self) -> Self::Output {
        SevenSegment { segments: self.segments ^ rhs.segments }
    }
}

impl Not for SevenSegment {
    type Output = SevenSegment;

    fn not(self) -> Self::Output {
        SevenSegment { segments: !self.segments }
    }
}

impl Sub for SevenSegment {
    type Output = SevenSegment;

    fn sub(self, rhs: Self) -> Self::Output {
        SevenSegment { segments: self.segments - (self & rhs).segments }
    }
}

const ZERO: SevenSegment = SevenSegment { segments: A as u8 | B as u8 | C as u8 | E as u8 | F as u8 | G as u8 }; // 0
const ONE: SevenSegment = SevenSegment { segments: C as u8 | F as u8 }; // 1
const TWO: SevenSegment = SevenSegment { segments: A as u8 | C as u8 | D as u8 | E as u8 | G as u8 }; // 2
const THREE: SevenSegment = SevenSegment { segments: A as u8 | C as u8 | D as u8 | F as u8 | G as u8 }; // 3
const FOUR: SevenSegment = SevenSegment { segments: B as u8 | C as u8 | D as u8 | F as u8 }; // 4
const FIVE: SevenSegment = SevenSegment { segments: A as u8 | B as u8 | D as u8 | F as u8 | G as u8 }; // 5
const SIX: SevenSegment = SevenSegment { segments: A as u8 | B as u8 | D as u8 | E as u8 | F as u8 | G as u8 }; // 6
const SEVEN: SevenSegment = SevenSegment { segments: A as u8 | C as u8 | F as u8 }; // 7
const EIGHT: SevenSegment = SevenSegment { segments: A as u8 | B as u8 | C as u8 | D as u8 | E as u8 | F as u8 | G as u8 }; // 8
const NINE: SevenSegment = SevenSegment { segments: A as u8 | B as u8 | C as u8 | D as u8 | E as u8 | F as u8 | G as u8 }; // 9
const DIGITS: [SevenSegment; 10] = [ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE];

fn problem_1() {
    let reader = BufReader::new(File::open("day8/input.txt").unwrap());

    let mut num_easy = 0;

    for line in reader.lines() {
        let line = line.unwrap();

        let (_, output_value) = line.split_once('|').unwrap();

        let output_digits: Vec<_> = output_value.trim().split(' ').collect();

        for d in output_digits {
            if [ONE.len(), FOUR.len(), SEVEN.len(), EIGHT.len()].contains(&d.len()) {
                num_easy += 1;
            }
        }
    }

    println!("P1: {}", num_easy);
}

fn problem_2() {
    let reader = BufReader::new(File::open("day8/input.txt").unwrap());

    let mut sum = 0;

    for line in reader.lines() {
        let line = line.unwrap();

        let (unique_combinations, output_value) = line.split_once('|').unwrap();

        let unique_combinations: Vec<_> = unique_combinations.trim().split(' ').map(SevenSegment::new).collect();
        let output_digits: Vec<_> = output_value.trim().split(' ').map(SevenSegment::new).collect();

        let one_digit =  unique_combinations.iter().copied().find(|digit| digit.len() == 2).unwrap();
        let four_digit =  unique_combinations.iter().copied().find(|digit| digit.len() == 4).unwrap();
        let seven_digit =  unique_combinations.iter().copied().find(|digit| digit.len() == 3).unwrap();
        let eight_digit =  unique_combinations.iter().copied().find(|digit| digit.len() == 7).unwrap();

        let a = seven_digit - one_digit;

        let nine_digit = unique_combinations.iter().copied().find(|digit| digit.len() == 6 && (*digit & four_digit) == four_digit).unwrap();
        let zero_digit = unique_combinations.iter().copied().find(|digit| digit.len() == 6 && *digit != nine_digit && (*digit & one_digit).len() == 2).unwrap();
        let six_digit = unique_combinations.iter().copied().find(|digit| digit.len() == 6 && *digit != nine_digit && *digit != zero_digit).unwrap();

        let e = eight_digit - nine_digit;
        let d = eight_digit - zero_digit;
        let b = four_digit - one_digit - d;
        let g = nine_digit - seven_digit - b - d;

        let two_digit = unique_combinations.iter().copied().find(|digit| digit.len() == 5 && (*digit & e).len() == 1 && (*digit & b).len() == 0).unwrap();

        let c = one_digit & two_digit;
        let f = one_digit - c;

        let three_digit = eight_digit - b - e;
        let five_digit = eight_digit - c - e;

        let digits = [zero_digit, one_digit, two_digit, three_digit, four_digit, five_digit, six_digit, seven_digit, eight_digit, nine_digit];

        let mut num = 0;
        for output_digit in output_digits.iter() {
            let digit_value = digits.iter().copied().enumerate().find(|(_, digit)| digit == output_digit).unwrap().0;
            num *= 10;
            num += digit_value;
        }

        sum += num;
        // println!("P2: digit = {}", num);
    }

    println!("P2: sum = {}", sum);
}
