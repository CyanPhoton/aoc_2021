use std::fs::File;
use std::io::{BufReader, Read, Seek};
use common::{BitIterator, bits_to_int, LimitIter, NextN};

fn main() {
    let packet = parse_message();

    dbg!(&packet);

    println!("P1: {}", packet.sum_version());

    println!("P2: {}", packet.eval());
}

const VERSION_SIZE: usize = 3;
const TYPE_ID_SIZE: usize = 3;
const HEADER_SIZE: usize = VERSION_SIZE + TYPE_ID_SIZE;

#[derive(Debug)]
struct Packet {
    pub version: u8,
    pub type_id: TypeID,
    pub type_e: PacketType
}

#[derive(Debug, Copy, Clone)]
#[repr(u8)]
enum TypeID {
    Sum = 0,
    Product = 1,
    Minimum = 2,
    Maximum = 3,
    Literal = 4,
    GreaterThan = 5,
    LessThan = 6,
    EqualTo = 7
}

#[derive(Debug)]
enum PacketType {
    Literal(u128),
    Operator {
        length_type: OperatorLengthType,
        sub_packets: Vec<Packet>
    }
}

const OP_BITS_LEN_LENGTH: usize = 15;
const OP_COUNT_LEN_LENGTH: usize = 11;

#[derive(Debug, Copy, Clone)]
enum OperatorLengthType {
    Bits(u16),
    Count(u16)
}

fn parse_message() -> Packet {
    let mut reader = BufReader::new(File::open("day16/input.txt").unwrap());

    let mut bits = (0..).map_while(|_| {
        let mut buf = [0; 2];
        match reader.read_exact(&mut buf[..]) {
            Ok(_) => Some((buf[0] as char, Some(buf[1] as char))),
            Err(_) => {
                if reader.stream_position().unwrap() % 2 == 1 {
                    reader.seek_relative(-1).unwrap();
                    let res = reader.read_exact(&mut buf[0..=0]).ok().map(|_| {
                        (buf[0] as char, None)
                    });
                    reader.seek_relative(1).unwrap(); // Make index even, so iterator stops
                    res
                } else {
                    None
                }
            }
        }
    })
        .map(|c| {
            let upper = c.0.to_digit(16).unwrap() as u8;
            let lower = c.1.map(|c| c.to_digit(16).unwrap() as u8);

            if let Some(lower) = lower {
                upper * 0x10u8 + lower
            } else {
                upper * 0x10u8 // Effectively pads end with 0 to give even number of hexadecimal digits
            }
        })
        .flat_map(|b| BitIterator::new(b).rev());

    Packet::parse(&mut bits).unwrap()
}

impl Packet {
    pub fn parse<I: Iterator<Item=u8>>(bits: &mut I) -> Option<Packet> {
        let version: u8 = bits_to_int(bits.next_n::<VERSION_SIZE>()?);
        let type_id: u8 = bits_to_int(bits.next_n::<TYPE_ID_SIZE>()?);
        // Since TypeID has values for 0..=7, will always be safe
        let type_id = unsafe { std::mem::transmute(type_id) };

        let type_e = match type_id {
            TypeID::Literal => {
                let mut v: u128 = 0;
                let mut count = 0;

                loop {
                    let last_segment = bits.next()? == 0;
                    count += 1;

                    let segment = bits.next_n::<4>()?;
                    let segment_value: u128 = bits_to_int(segment);

                    v = v.checked_mul(1 << 4).unwrap();
                    v |= segment_value;

                    if last_segment {
                        break;
                    }
                }

                PacketType::Literal(v)
            },
            _ => {
                let length_type = match bits.next()? {
                    0 => OperatorLengthType::Bits(bits_to_int(bits.next_n::<OP_BITS_LEN_LENGTH>()?)),
                    1 => OperatorLengthType::Count(bits_to_int(bits.next_n::<OP_COUNT_LEN_LENGTH>()?)),
                    _ => unreachable!("A bit can only be 1 or 0")
                };

                let sub_packets = match length_type {
                    OperatorLengthType::Bits(bit_count) => {
                        // Need to collect, else infinitely recursive type that never compiles
                        let sub_bits = LimitIter::new(bits, bit_count as usize).collect::<Vec<_>>();

                        let mut sub_bits = sub_bits.into_iter();
                        (0..).map_while(|_| Self::parse(&mut sub_bits)).collect::<Vec<_>>()
                    }
                    OperatorLengthType::Count(count) => {
                        (0..count).map(|_| Self::parse(bits).unwrap()).collect::<Vec<_>>()
                    }
                };

                PacketType::Operator {
                    length_type,
                    sub_packets
                }
            }
        };

        Some(Packet {
            version,
            type_id,
            type_e,
        })
    }

    pub fn sum_version(&self) -> usize {
        self.version as usize
            + match &self.type_e {
            PacketType::Literal(_) => { 0 }
            PacketType::Operator{ sub_packets, .. } => sub_packets.iter().map(Self::sum_version).sum::<usize>()
        }
    }

    pub fn eval(&self) -> u128 {
        match &self.type_e {
            PacketType::Literal(v) => *v,
            PacketType::Operator { sub_packets, .. } => match self.type_id {
                TypeID::Sum => sub_packets.iter().map(Self::eval).sum::<u128>(),
                TypeID::Product => sub_packets.iter().map(Self::eval).product::<u128>(),
                TypeID::Minimum => sub_packets.iter().map(Self::eval).min().unwrap(),
                TypeID::Maximum => sub_packets.iter().map(Self::eval).max().unwrap(),
                TypeID::GreaterThan => if sub_packets[0].eval() > sub_packets[1].eval() { 1 } else { 0 },
                TypeID::LessThan => if sub_packets[0].eval() < sub_packets[1].eval() { 1 } else { 0 }
                TypeID::EqualTo => if sub_packets[0].eval() == sub_packets[1].eval() { 1 } else { 0 }
                TypeID::Literal => unreachable!(),
            },
        }
    }
}