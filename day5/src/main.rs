use std::cmp::max;
use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::AddAssign;
use std::path::{Path, PathBuf};

fn main() {
    problem_1();
    problem_2();
}

type PointType = i32;
type Point = (PointType, PointType);

struct Line {
    start: Point,
    end: Point,
}

impl Line {
    pub fn is_vertical(&self) -> bool {
        self.start.0 == self.end.0
    }

    pub fn is_horizontal(&self) -> bool {
        self.start.1 == self.end.1
    }

    pub fn is_diagonal(&self) -> bool {
        !self.is_vertical() && !self.is_horizontal()
    }

    pub fn is_axis_aligned(&self) -> bool {
        self.is_vertical() || self.is_horizontal()
    }

    pub fn iter_points(&self) -> impl Iterator<Item=Point> {
        let (x_step, x_size) = if self.is_vertical() { (0, 1) } else if self.start.0 < self.end.0 { (1, self.end.0 - self.start.0 + 1) } else { (-1, self.start.0 - self.end.0 + 1) };
        let (y_step, y_size) = if self.is_horizontal() { (0, 1) } else if self.start.1 < self.end.1 { (1, self.end.1 - self.start.1 + 1) } else { (-1, self.start.1 - self.end.1 + 1) };
        let size = x_size.max(y_size);

        let mut p = self.start;
        (0..size).map(move |_| {
            let point = p;
            p.0 += x_step;
            p.1 += y_step;
            point
        })
    }
}

struct Board {
    map: HashMap<Point, usize>,
    max: Point,
    max_count: usize
}

impl Board {
    pub fn new<I: IntoIterator<Item=Line>>(lines: I) -> Board {
        let mut map = HashMap::new();
        let mut max = (0, 0);
        let mut max_count = 0;

        for line in lines.into_iter() {
            for p in line.iter_points() {
                let mut entry = map.entry(p).or_insert(0);
                entry.add_assign(1);
                max_count = max_count.max(*entry);
                max = (max.0.max(p.0), max.1.max(p.1))
            }
        }

        Board {
            map,
            max,
            max_count
        }
    }

    pub fn iter_points(&self) -> impl Iterator<Item=(&Point, &usize)> {
        self.map.iter().map(|p| p)
    }

    pub fn write_to_image<P: AsRef<Path>>(&self, path: P) {
        let mut image_buffer = image::ImageBuffer::new((self.max.0 + 1) as u32, (self.max.1 + 1) as u32);

        let map_colour = |count: usize| {
            if count == 0 {
                image::Rgb([0, 0, 0])
            } else {
                let r = (((count as f32 / self.max_count as f32).powf(2.0) * 255.0) as u8).min(255);
                image::Rgb([r, r, r])
            }
        };

        for (x, y, pixel) in image_buffer.enumerate_pixels_mut() {
            *pixel = map_colour(self.map.get(&(x as PointType, y as PointType)).copied().unwrap_or(0));
        }

        image_buffer.save(path).unwrap()
    }
}

impl Debug for Board {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for y in 0..=self.max.1 {
            for x in 0..=self.max.0 {
                if let Some(c) = self.map.get(&(x, y)) {
                    write!(f, "{}", c)?;
                } else {
                    write!(f, ".")?;
                }
            }
            writeln!(f, "")?;
        }

        Ok(())
    }
}

fn map_pair<U, V, F: FnMut(U) -> V>(pair: (U, U), mut f: F) -> (V, V) {
    (f(pair.0), f(pair.1))
}

fn read_lines() -> Vec<Line> {
    let reader = BufReader::new(File::open("day5/input.txt").unwrap());

    reader.lines().map(|line| {
        let line = line.unwrap();

        let points = line.split_once("->").unwrap();
        let (start, end)  = map_pair(points, |p| p.trim().split_once(',').unwrap());

        let parse_coord = |p: (&str, &str)| -> Point {
            map_pair(p, |v| v.parse::<PointType>().unwrap())
        };

        Line {
            start: parse_coord(start),
            end: parse_coord(end)
        }
    }).collect()
}

fn problem_1() {
    let board = Board::new(read_lines().into_iter().filter(|l| l.is_axis_aligned()));

    let dangerous = board.iter_points().filter(|p| *p.1 >= 2).count();
    println!("P1: Dangerous point count: {}", dangerous);
    // println!("P1: Board:\n{:?}", board);
    board.write_to_image("day5/p1.png");
}

fn problem_2() {
    let board = Board::new(read_lines());

    let dangerous = board.iter_points().filter(|p| *p.1 >= 2).count();
    println!("P2: Dangerous point count: {}", dangerous);
    // println!("P2: Board:\n{:?}", board);
    board.write_to_image("day5/p2.png");
}
