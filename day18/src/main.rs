use std::fmt::{Debug, Formatter};
// use std::collections::LinkedList;
use linked_list::LinkedList; // Note: This is since the std cursor is unstable (still)
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let reader = BufReader::new(File::open("day18/input.txt").unwrap());

    let numbers: Vec<_> = reader.lines().map(|l| Number::parse_line(l.unwrap())).collect();

    println!("Numbers: {:?}", numbers);

    let sum = numbers.clone().into_iter().fold(Option::<Number>::None, |sum, n| {
        if let Some(mut sum) = sum {
            // println!("Adding: {:?}", sum);
            // println!("and   : {:?}", n);
            sum.add_assign(n);
            // println!("Partial sum: {:?}", sum);
            Some(sum)
        } else {
            Some(n)
        }
    }).unwrap();

    println!("Sum: {:?}", sum);
    println!("P1: {:?}", sum.calc_magnitude());

    let n = &numbers;

    let max_mag = (0..n.len()).flat_map(|i| {
        (0..n.len()).filter_map(move |j| {
            if i == j {
                None
            } else {
                let mut x = n[i].clone();
                let y = n[j].clone();

                x.add_assign(y);

                Some(x.calc_magnitude())
            }
        })
    }).max().unwrap();

    println!("P2: {:?}", max_mag);
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum NumberToken {
    Number(u8),
    Separator,
    OpenPair,
    ClosePair
}

#[derive(Clone)]
struct Number {
    tokens: LinkedList<NumberToken>
}

impl Debug for Number {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for t in &self.tokens {
            match t {
                NumberToken::Number(n) => write!(f, "{}", n)?,
                NumberToken::Separator => write!(f, ",")?,
                NumberToken::OpenPair => write!(f, "[")?,
                NumberToken::ClosePair => write!(f, "]")?,
            }
        }

        Ok(())
    }
}

impl Number {
    pub fn parse_line(line: String) -> Number {
        let tokens = line.chars().map(|c| {
            if let Some(digit) = c.to_digit(10) {
                NumberToken::Number(digit as u8)
            } else {
                match c {
                    ',' => NumberToken::Separator,
                    '[' => NumberToken::OpenPair,
                    ']' => NumberToken::ClosePair,
                    _ => panic!("Unexpected token: {}", c)
                }
            }
        }).collect();

        Number {
            tokens
        }
    }

    pub fn add_assign(&mut self, mut rhs: Number) {
        self.tokens.push_front(NumberToken::OpenPair);
        self.tokens.push_back(NumberToken::Separator);
        self.tokens.append(&mut rhs.tokens);
        self.tokens.push_back(NumberToken::ClosePair);

        // println!("\tAfter sum: {:?}", self);

        self.reduce();
    }

    pub fn calc_magnitude(&self) -> u64 {
        const LEFT_MUL: u64 = 3;
        const RIGHT_MUL: u64 = 2;

        let mut mag = 0;
        let mut multiplier = 1;

        for t in self.tokens.iter() {
            match *t {
                NumberToken::Number(n1) => mag += (n1 as u64) * multiplier,
                NumberToken::Separator => {
                    multiplier /= LEFT_MUL;
                    multiplier *= RIGHT_MUL;
                }
                NumberToken::OpenPair => {
                    multiplier *= LEFT_MUL;
                }
                NumberToken::ClosePair => {
                    multiplier /= RIGHT_MUL;
                }
            }
        }

        mag
    }

    // Assumes that max depth is 5, aka runs after at most 1 addition of another number, where both have been reduced
    fn reduce(&mut self) {
        'outer: loop {
            let mut cursor = self.tokens.cursor();

            let mut depth = 0;

            // Check for need to "explode"
            while let Some(t) = cursor.next() {
                if *t == NumberToken::OpenPair {
                    depth += 1;
                } else if *t == NumberToken::ClosePair {
                    depth -= 1;
                }
                // Found pair that needs "explode"ing
                if depth == 5 {
                    // Move back before [L,R]
                    cursor.seek_backward(1);
                    depth -= 1;
                    cursor.remove().unwrap(); // Remove [
                    let l = cursor.remove().unwrap(); // Remove L
                    cursor.remove().unwrap(); // Remove ,
                    let r = cursor.remove().unwrap(); // Remove R
                    cursor.remove().unwrap(); // Remove ]

                    let l = if let NumberToken::Number(n) = l { n } else { panic!("Unexpected token") };
                    let r = if let NumberToken::Number(n) = r { n } else { panic!("Unexpected token") };

                    // println!("\t\tExploding: [{},{}]", l, r);

                    cursor.insert(NumberToken::Number(0)); // [[A,B],C] -> [0,C]

                    // Seek left to add l to
                    let mut offset = 1;
                    loop {
                        offset += 1;
                        if let Some(nl) = cursor.prev() {
                            if let NumberToken::Number(n) = nl {
                                *n = n.checked_add(l).unwrap();
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    cursor.seek_forward(offset);

                    // Seek right to add r to
                    let mut offset = 1;
                    cursor.seek_forward(1); // Skip over the number 0 that was added
                    loop {
                        offset += 1;
                        if let Some(nr) = cursor.next() {
                            if let NumberToken::Number(n) = nr {
                                *n = n.checked_add(r).unwrap();
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    cursor.seek_backward(offset);
                }
            }
            // Done all "explode"ing

            // println!("\tAfter exploding: {:?}", self);
            // let mut cursor = self.tokens.cursor();

            // Check for need to "split"
            depth = 0;
            let mut split = 0;
            while let Some(t) = cursor.next() {
                if *t == NumberToken::OpenPair {
                    depth += 1;
                } else if *t == NumberToken::ClosePair {
                    depth -= 1;
                }

                if let NumberToken::Number(n) = *t {
                    if n >= 10 {
                        split += 1;
                        let l = n / 2;
                        let r = (n + 1) / 2;

                        // println!("\t\tSplitting: {}, at depth = {}", n, depth);

                        cursor.seek_backward(1); // Move before >= 10
                        cursor.remove(); // Remove >= 10

                        // Note: Insert in reverse since cursor stays before the inserted
                        cursor.insert(NumberToken::ClosePair); // Add ]
                        cursor.insert(NumberToken::Number(r)); // Add R
                        cursor.insert(NumberToken::Separator); // Add ,
                        cursor.insert(NumberToken::Number(l)); // Add L
                        cursor.insert(NumberToken::OpenPair); // Add [

                        if depth == 4 {
                            continue 'outer; // Restart other loop if we created a pair in need to "exploding"
                        }
                    }
                }
            }

            if split == 0 {
                break;
            }
        }
    }
}
