use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use fxhash::{FxHashMap, FxHashSet};
use positronium_maths::{Matrix3i, Vector, Vector3i};
use common::NextN;

const ROTATIONS: usize = 24;
const MATCH_SET_SIZE: usize = 12;

fn main() {
    let mut data = load_data();
    assert_eq!(data[0].id, 0);

    let matrices= calc_rotation_matrices();
    assert_eq!(matrices.len(), ROTATIONS);
    assert_eq!(matrices[0], Matrix3i::identity());

    for data in &mut data {
        data.calc_possible_match_data(&matrices);
    }

    let mut arranged = {
        let first = data.swap_remove(0);
        vec![
            ArrangedData {
                id: first.id,
                offset: Vector3i::zero(),
                relative_positions: first.relative_positions.iter().copied().collect()
            }]
    };

    let mut to_arrange: Vec<_> = data;

    let mut checked_pairs = FxHashSet::default();

    'outer: while !to_arrange.is_empty() {
        for a in &arranged {
            for i in 0..to_arrange.len() {
                let option = &to_arrange[i];

                if checked_pairs.contains(&(a.id, option.id)) {
                    continue;
                }

                if let Some((offset, rot)) = a.check_possible_match(option) {
                    let option = to_arrange.swap_remove(i);

                    let a_id = a.id;
                    arranged.push(ArrangedData {
                        id: option.id,
                        offset: offset + a.offset,
                        relative_positions: option.relative_positions.into_iter().map(|v| matrices[rot] * v).collect()
                    });
                    println!("Arranged: {} from {}", option.id, a_id);

                    continue 'outer;
                } else {
                    checked_pairs.insert((a.id, option.id));
                    checked_pairs.insert((option.id, a.id));
                }
            }
        }

        dbg!(&arranged);
        panic!("No more options");
    }

    // dbg!(&arranged);
    println!("Done arranging");

    let positions: FxHashSet<_> = arranged.iter().flat_map(|a| a.relative_positions.iter().copied().map(|r| r + a.offset)).collect();

    println!("P1: Total positions: {} = {:?}", positions.len(), positions);

    let largest_separation = (0..arranged.len()).flat_map(|i| (0..i).map(move |j| (i, j))).map(|(i, j)| {
        (arranged[i].offset - arranged[j].offset).into_array().map(|v| v.abs()).into_iter().sum::<i32>()
    }).max().unwrap();

    println!("P2: {}", largest_separation);
}

fn calc_rotation_matrices() -> Vec<Matrix3i> {
    let dirs = [Vector3i::unit_x(), Vector3i::unit_y(), Vector3i::unit_z()];
    let mut matrices = Vec::new();

    for i in 0..6 {
        let look_dir = dirs[i % 3] * (1 - 2 * (i as i32 / 3));
        for j in 0..6 {
            if i % 3 == j % 3 {
                continue;
            }

            let up_dir = dirs[j % 3] * (1 - 2 * (j as i32 / 3));
            let right_dir = look_dir ^ up_dir;

            let matrix = Matrix3i::new(look_dir, up_dir, right_dir);
            matrices.push(matrix);
        }
    }

    matrices
}

#[derive(Debug)]
struct ArrangedData {
    id: usize,
    offset: Vector3i,
    relative_positions: FxHashSet<Vector3i>
}

#[derive(Default)]
struct PossibleMatchData {
    self_offset_set: FxHashSet<Vector3i>,
    // Relative offsets, each transformed by a rotation matrix
    per_rotation_ordered_offsets: Vec<Vec<Vector3i>>
}

struct ScannerData {
    id: usize,
    relative_positions: Vec<Vector3i>,
    possible_match_data: PossibleMatchData
}

impl ScannerData {
    pub fn new(id: usize) -> ScannerData {
        ScannerData {
            id,
            relative_positions: vec![],
            possible_match_data: PossibleMatchData::default()
        }
    }

    pub fn calc_possible_match_data(&mut self, matrices: &Vec<Matrix3i>) {
        self.possible_match_data.self_offset_set = self.relative_positions.iter().copied().collect();

        for matrix in matrices.iter().copied() {
            let offsets = self.relative_positions.iter().copied().map(|v| matrix * v).collect();
            self.possible_match_data.per_rotation_ordered_offsets.push(offsets);
        }
    }
}

impl ArrangedData {
    pub fn check_possible_match(&self, other: &ScannerData) -> Option<(Vector3i, usize)> {
        for r in 0..ROTATIONS {
            for i in self.relative_positions.iter().copied() {
                for j in other.possible_match_data.per_rotation_ordered_offsets[r].iter().copied() {
                    // (i, j) potential common points
                    // Potential offset of other's centre compare to self centre, in self rotation
                    let offset = i + (-j);

                    let j_set = other.possible_match_data.per_rotation_ordered_offsets[r].iter().copied()
                        .map(|o| o + offset).collect::<FxHashSet<_>>();

                    if self.relative_positions.intersection(&j_set).count() >= MATCH_SET_SIZE {
                        return Some((offset, r));
                    }
                }
            }
        }

        None
    }
}

fn load_data() -> Vec<ScannerData> {
    let reader = BufReader::new(File::open("day19/input.txt").unwrap());

    let mut data = Vec::new();

    let mut working_data = None;
    for line in reader.lines().map(|l| l.unwrap()).filter(|l| !l.is_empty()) {
        if line.starts_with("---") {
            if let Some(working_data) = working_data.take() {
                data.push(working_data);
            }

            let (_, id) = line.trim_matches('-').trim().split_once(' ').unwrap();
            let id = id.parse::<usize>().unwrap();
            working_data = Some(ScannerData::new(id));
        } else {
            let rel = line.split(',').next_n().unwrap().map(|i| i.parse().unwrap());
            working_data.as_mut().unwrap().relative_positions.push(Vector::from(rel));
        }
    }

    if let Some(working_data) = working_data {
        data.push(working_data);
    }

    data
}
