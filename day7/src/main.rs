use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    problem_1();
    println!("-------------------------------------------------");
    problem_2();
}

// Apparently the answer is just the median, and assuming there is only 1 minimum I can see that is correct in the math
// My solution is not O(n) like an optimal median calc, rather O(m+n) where is the max position, for dense positions this is O(n), but otherwise mine is generally worse
fn problem_1() {
    let reader = BufReader::new(File::open("day7/input.txt").unwrap());
    let line = reader.lines().next().unwrap().unwrap();

    let positions: Vec<_> = line.split(',').map(|p| p.parse::<u16>().unwrap()).collect();
    let position_count = positions.iter().copied().max().unwrap() as usize + 1;

    // The number at index i is the number of crabs in position i
    let mut position_counts = vec![0u64; position_count];
    for pos in positions {
        position_counts[pos as usize] += 1;
    }

    // The number at index i is the sum of crabs in positions < i
    let left_counts: Vec<_> = {
        let mut left_count = 0;
        position_counts.iter().copied().map(|p| {
            let count = left_count;
            left_count += p;
            count
        }).collect()
    };

    // The number at index i is the sum of crabs in positions > i
    let mut right_counts: Vec<_> = {
        let mut right_count = 0;
        position_counts.iter().rev().copied().map(|p| {
            let count = right_count;
            right_count += p;
            count
        }).collect()
    };
    right_counts.reverse();

    let deltas: Vec<_> = (0..position_count-1).map(|n| left_counts[n] as i64 + position_counts[n] as i64 - right_counts[n] as i64).collect();

    let partial_sums: Vec<_> = {
        let mut partial_sum = 0;

        std::iter::once(0).chain(deltas.iter().copied()).map(|d| {
            partial_sum += d;
            partial_sum
        }).collect()
    };

    let (min_position, _) = partial_sums.iter().enumerate().min_by_key(|p| p.1).unwrap();

    let cost: u64 = position_counts.iter().copied().enumerate().map(|(i, n)| (min_position as i64 - i as i64).abs() as u64 * n).sum();

    println!("P1: position_counts: {:?}", position_counts);
    println!("P1: left_counts:     {:?}", left_counts);
    println!("P1: right_counts:    {:?}", right_counts);
    println!("P1: deltas:          {:?}", deltas);
    println!("P1: partial_sums:    {:?}", partial_sums);
    println!("P1: min_position:    {:?}", min_position);
    println!("P1: cost:            {:?}", cost);
}

// Apparently the answer is just the mean, and assuming there is only 1 minimum I can see that is correct in the math
// My solution is not O(n) like an optimal mean calc, rather O(m+n) where is the max position, for dense positions this is O(n), but otherwise mine is generally worse
fn problem_2() {
    let reader = BufReader::new(File::open("day7/input.txt").unwrap());
    let line = reader.lines().next().unwrap().unwrap();

    let positions: Vec<_> = line.split(',').map(|p| p.parse::<u16>().unwrap()).collect();
    let position_count = positions.iter().copied().max().unwrap() as usize + 1;

    // The number at index i is the number of crabs in position i
    let mut position_counts = vec![0u64; position_count];
    for pos in positions {
        position_counts[pos as usize] += 1;
    }

    // The number at index i is the sum of crabs in positions < i
    let left_counts: Vec<_> = {
        let mut left_count = 0;
        position_counts.iter().copied().map(|p| {
            let count = left_count;
            left_count += p;
            count
        }).collect()
    };

    // Math magic
    let second_left_counts: Vec<_> = {
        let mut second_left_count = 0;
        position_counts.iter().copied().enumerate().map(|(i, p)| {
            second_left_count += left_counts[i];
            let count = second_left_count;
            second_left_count += p;
            count
        }).collect()
    };

    // The number at index i is the sum of crabs in positions > i
    let mut right_counts: Vec<_> = {
        let mut right_count = 0;
        position_counts.iter().rev().copied().map(|p| {
            let count = right_count;
            right_count += p;
            count
        }).collect()
    };
    right_counts.reverse();

    // Math magic
    let mut second_right_counts: Vec<_> = {
        let mut second_right_count = 0;
        position_counts.iter().copied().enumerate().rev().map(|(i, _p)| {
            second_right_count += right_counts[i];
            let count = second_right_count;
            count
        }).collect()
    };
    second_right_counts.reverse();

    let deltas: Vec<_> = (0..position_count-1).map(|n| second_left_counts[n] as i64 + position_counts[n] as i64 - second_right_counts[n] as i64).collect();

    let partial_sums: Vec<_> = {
        let mut partial_sum = 0;

        std::iter::once(0).chain(deltas.iter().copied()).map(|d| {
            partial_sum += d;
            partial_sum
        }).collect()
    };

    let (min_position, _) = partial_sums.iter().enumerate().min_by_key(|p| p.1).unwrap();

    let f = |d: u64| (d * d + d) / 2;
    let cost_0: u64 = position_counts.iter().copied().enumerate().map(|(i, n)| f(i as u64) * n).sum();

    let costs: Vec<_> = partial_sums.iter().copied().map(|s| (s + cost_0 as i64) as u64).collect();

    println!("P2: position_counts:     {:?}", position_counts);
    println!("P2: left_counts:         {:?}", left_counts);
    println!("P2: second_left_counts:  {:?}", second_left_counts);
    println!("P2: right_counts:        {:?}", right_counts);
    println!("P2: second_right_counts: {:?}", second_right_counts);
    println!("P2: deltas:              {:?}", deltas);
    println!("P2: partial_sums:        {:?}", partial_sums);
    println!("P2: min_position:        {:?}", min_position);
    println!("P2: costs:               {:?}", costs);
    println!("P2: min cost:            {:?}", costs[min_position]);
}
