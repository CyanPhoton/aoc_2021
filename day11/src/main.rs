use std::fmt::{Debug, Formatter};
use std::fs::File;
use std::io::{BufRead, BufReader};
use common::*;

fn main() {
    problem_1();
    problem_2();
}

const SIZE: usize = 10;

pub struct Grid {
    grid: [[u8; SIZE]; SIZE]
}

impl Grid {
    fn for_neighbours<F: FnMut(usize, usize)>(r: usize, c: usize, mut f: F) {
        for ar in r.saturating_sub(1)..(r + 2).min(SIZE) {
            for ac in c.saturating_sub(1)..(c + 2).min(SIZE) {
                if !(ar == r && ac == c) {
                    f(ar, ac);
                }
            }
        }
    }

    pub fn step(&mut self) -> usize {
        let mut flashes = 0;

        for r in 0..SIZE {
            for c in 0..SIZE {
                self.grid[r][c] += 1;
            }
        }

        loop {
            let old_flashes = flashes;

            for r in 0..SIZE {
                for c in 0..SIZE {
                    if self.grid[r][c] >= 10 {
                        flashes += 1;
                        self.grid[r][c] = 0;

                        Self::for_neighbours(r, c, |r, c| {
                            if self.grid[r][c] != 0 {
                                self.grid[r][c] += 1;
                            }
                        })
                    }
                }
            }

            if old_flashes == flashes {
                break;
            }
        }

        flashes
    }
}

impl Debug for Grid {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for r in 0..SIZE {
            for c in 0..SIZE {
                write!(f, "{}", self.grid[r][c])?;
            }
            writeln!(f, "")?;
        }

        Ok(())
    }
}

fn problem_1() {
    let reader = BufReader::new(File::open("day11/input.txt").unwrap());

    let mut grid = Grid {
        grid: reader.lines().map(|l| l.unwrap().chars().map(|c| c.to_digit(10).unwrap() as u8).collect_n().unwrap()).collect_n().unwrap()
    };

    println!("Before any steps: \n{:?}\n", grid);

    let mut flashes = 0;
    for i in 0..100 {
        flashes += grid.step();
        if i < 10 || (i + 1) % 10 == 0 {
            println!("After step {}: \n{:?}\n", i + 1, grid);
        }
    }

    println!("P1: flashes = {}", flashes);
}

fn problem_2() {
    let reader = BufReader::new(File::open("day11/input.txt").unwrap());

    let mut grid = Grid {
        grid: reader.lines().map(|l| l.unwrap().chars().map(|c| c.to_digit(10).unwrap() as u8).collect_n().unwrap()).collect_n().unwrap()
    };

    for i in 0.. {
        let flashes = grid.step();
        if flashes == SIZE*SIZE {
            println!("P2: All flashed after step: {}", i + 1);
            break;
        }
    }

}
