use std::fs::File;
use std::io::{BufRead, BufReader};
use std::mem::{ManuallyDrop, MaybeUninit};
use common::*;

fn main() {
    problem_1();
    problem_2();
}

const BOARD_SIZE: usize = 5;

struct Slot {
    choice: u8,
    marked: bool
}

struct Board {
    // [Row][Column], so stored row wise, not that it should really matter
    numbers: [[Slot; BOARD_SIZE]; BOARD_SIZE]
}

fn read_input() -> (Vec<Board>, Vec<u8>) {
    let reader = BufReader::new(File::open("day4/input.txt").unwrap());
    let mut lines = reader.lines().filter_map(|line| {
        let line = line.unwrap();
        if line.is_empty() {
            None
        } else {
            Some(line)
        }
    });

    let drawn_numbers: Vec<_> = lines.next().unwrap().split(',').map(|s| s.parse::<u8>().unwrap()).collect();

    let mut boards = Vec::new();

    while let Some(board_lines) = lines.next_n::<BOARD_SIZE>() {
        boards.push(Board {
            numbers: board_lines.map(|board_line| {
                board_line.split_whitespace().map(|entry| {
                    Slot {
                        choice: entry.parse::<u8>().unwrap(),
                        marked: false
                    }
                }).collect_n().unwrap()
            })
        });
    }

    (boards, drawn_numbers)
}

// Since no stable way to do this yet
pub fn vec_retain_mut<T, R: FnMut(&mut T) -> bool>(vec: &mut Vec<T>, mut retain: R) {
    // Safe since ManuallyDrop<T> and (T) have the same layout
    let vec: &mut Vec<ManuallyDrop<T>> = unsafe { std::mem::transmute(vec) };

    // If < j, then points to the first 'empty' slot one can move into
    let mut i = 0;
    // Points to the next valid value to check, or one beyond to indicate finished
    let mut j = 0;

    while j < vec.len() {
        // Safe since ManuallyDrop<T> and (T) have the same layout, and we make sure that j always points to valid entries
        let v: &mut T = unsafe { std::mem::transmute(&mut vec[j]) };

        if retain(v) {
            if i < j {
                // If first 'empty' is before this one we want to retain, copy it left
                vec.swap(i, j);
            }
            // Next 'emtpy' will be on over
            i += 1;
            // Next valid (if any) will be 1 over
            j += 1;
        } else {

            // From take version
            // Safe since from reference
            // let v = unsafe { std::ptr::read(v) };
            //
            // take(v);

            // Safe since from reference
            unsafe { std::ptr::drop_in_place(v); }

            // Next valid (if any) will be 1 over
            j += 1;
        }
    }

    // I points to first empty, thus is number of valid elements to the left
    // truncate works here since it's ManuallyDrop
    vec.truncate(i);
}



impl Board {
    // Marks number, returns true if board is winner
    pub fn apply_number(&mut self, drawn: u8) -> bool {
        for r in 0..BOARD_SIZE {
            for c in 0..BOARD_SIZE {
                if self.numbers[r][c].choice == drawn {
                    self.numbers[r][c].marked = true;
                }
            }
        }

        // Check row matches
        for r in 0..BOARD_SIZE {
            if self.numbers[r].iter().all(|slot| slot.marked) {
                return true;
            }
        }

        // Check column matches
        for c in 0..BOARD_SIZE {
            if self.numbers.iter().all(|row| row[c].marked) {
                return true;
            }
        }

        false
    }

    pub fn sum_unmarked(&self) -> u32 {
        self.numbers.iter()
            .map(|row|
                row.iter()
                    .filter(|slot| !slot.marked)
                    .map(|slot| slot.choice as u32)
                    .sum::<u32>()
            ).sum()
    }
}

fn problem_1() {
    let (mut boards, drawn) = read_input();

    for drawn in drawn {
        for (i, board) in boards.iter_mut().enumerate() {
            let win = board.apply_number(drawn);
            if win {
                let score = (drawn as u32) * board.sum_unmarked();
                println!("P1: Winning board is #{}, with score: {}", i, score);
                return;
            }
        }
    }
}

fn problem_2() {
    let ( boards, drawn) = read_input();
    let mut boards: Vec<_> = boards.into_iter().enumerate().collect();

    let total_boards = boards.len();
    let mut winners = 0;
    for drawn in drawn {
        vec_retain_mut(&mut boards, |(i, board)| {
            let win = board.apply_number(drawn);
            if win {
                winners += 1;
                if winners == total_boards {
                    true // Don't drop this once, since it's the last winner
                } else {
                    false
                }
            } else {
                true
            }
        });

        if winners == total_boards {
            let (i, winner) = &boards[0];
            let score = (drawn as u32) * winner.sum_unmarked();
            println!("P2: Loosing board is #{}, with score: {}", i, score);
            return;
        }
    }
}
