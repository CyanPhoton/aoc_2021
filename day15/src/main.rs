use std::cmp::Ordering;
use std::collections::{BinaryHeap, BTreeMap, BTreeSet};
use std::fs::File;
use std::io::{BufRead, BufReader};
use arrayvec::ArrayVec;
use petgraph::{Graph, Undirected};
use petgraph::data::FromElements;
use petgraph::prelude::{GraphMap, DiGraphMap};

fn main() {
    problem_1();
    problem_2();
}

fn problem_1() {
    let reader = BufReader::new(File::open("day15/input.txt").unwrap());

    let risk: Vec<Vec<_>> = reader.lines().map(|l| l.unwrap()).map(|l| {
        l.chars().map(|c| c.to_digit(10).unwrap() as u8).collect()
    }).collect();

    let rows = risk.len();
    let columns = risk[0].len();

    let for_neighbours = |(r, c): (usize, usize)| {
        let mut neighbours = ArrayVec::<_, 4>::new();

        if r > 0 {
            neighbours.push((r - 1, c));
        }
        if c > 0 {
            neighbours.push((r, c - 1));
        }
        if r + 1 < rows {
            neighbours.push((r + 1, c));
        }
        if c + 1 < columns {
            neighbours.push((r, c + 1));
        }

        neighbours.into_iter()
    };

    let r = &risk;

    let graph = DiGraphMap::from_edges(
        (0..rows).flat_map(|r| (0..columns).map(move |c| (r, c))).flat_map(|p| {
            for_neighbours(p).map(move |n|  {
                (p, n, r[n.0][n.1] as usize)
            })
        })
    );

    let end = (rows - 1, columns - 1);

    let paths = petgraph::algo::dijkstra(&graph, (0, 0), Some(end), |e| *e.2);

    println!("P1: {}", paths[&end]);
}

fn problem_2() {
    let reader = BufReader::new(File::open("day15/input.txt").unwrap());

    let risk: Vec<Vec<_>> = reader.lines().map(|l| l.unwrap()).map(|l| {
        l.chars().map(|c| c.to_digit(10).unwrap() as u8).collect()
    }).collect();

    let rows = risk.len();
    let columns = risk[0].len();

    const TILES_X: usize = 5;
    const TILES_Y: usize = 5;

    let for_neighbours = |(r, c): (usize, usize)| {
        let mut neighbours = ArrayVec::<_, 4>::new();

        if r > 0 {
            neighbours.push((r - 1, c));
        }
        if c > 0 {
            neighbours.push((r, c - 1));
        }
        if r + 1 < rows * TILES_Y {
            neighbours.push((r + 1, c));
        }
        if c + 1 < columns * TILES_X {
            neighbours.push((r, c + 1));
        }

        neighbours.into_iter()
    };

    let r = &risk;

    let graph = DiGraphMap::from_edges(
        (0..(rows * TILES_Y)).flat_map(|r| (0..(columns * TILES_X)).map(move |c| (r, c))).flat_map(|p| {
            for_neighbours(p).map(move |n|  {
                (p, n, (((r[n.0 % rows][n.1 % columns] as usize + (n.0 / rows) + (n.1 / columns)) - 1) % 9) + 1)
            })
        })
    );

    let end = (rows * TILES_Y - 1, columns * TILES_X - 1);

    let paths = petgraph::algo::dijkstra(&graph, (0, 0), Some(end), |e| *e.2);

    println!("P2: {}", paths[&end]);
}
