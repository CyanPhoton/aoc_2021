use std::cmp::{Ordering, Reverse};
use std::collections::HashSet;
use std::fmt::{Debug, Display, Formatter};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;
use fxhash::{FxHashMap, FxHashSet};
use smallvec::SmallVec;
use common::CollectN;

fn main() {
    let start_burrow_p1 = Burrow::load(1);
    println!("P1: {:?}", solve(start_burrow_p1));
    let start_burrow_p2 = Burrow::load(2);
    println!("P2: {:?}", solve(start_burrow_p2));
}

fn solve(start_burrow: Burrow) -> Option<u64> {
    println!("start = {:?}", start_burrow);
    let mut visited = FxHashMap::<Burrow, u64>::default();
    let mut frontier = std::collections::binary_heap::BinaryHeap::<Reverse<BurrowStep>>::new();
    frontier.push(Reverse(BurrowStep { cost: 0, current: start_burrow }));

    while let Some(next) = frontier.pop().map(|n| n.0) {
        if next.current.is_final() {
            return Some(next.cost);
        }

        let possible = next.current.enumerate_possible_steps();

        frontier.extend(possible.into_iter().filter_map(|(cost, new)| {
            let new_cost = next.cost + cost;

            let mut inserted = false;
            let last_cost = visited.entry(new.clone()).or_insert_with(|| {
                inserted = true;
                new_cost
            });

            if inserted || new_cost < *last_cost {
                *last_cost = new_cost;
                Some(Reverse(BurrowStep { cost: new_cost, current: new }))
            } else {
                None
            }
        }))
    }

    return None;
}

#[derive(Eq, PartialEq, Copy, Clone, Debug, Hash, Ord, PartialOrd)]
enum AmphipodTypes {
    A = 1,
    B = 2,
    C = 3,
    D = 4
}

impl AmphipodTypes {
    pub fn movement_cost(&self) -> u64 {
        match self {
            AmphipodTypes::A => 1,
            AmphipodTypes::B => 10,
            AmphipodTypes::C => 100,
            AmphipodTypes::D => 1000,
        }
    }
}

impl FromStr for AmphipodTypes {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" => Ok(Self::A),
            "B" => Ok(Self::B),
            "C" => Ok(Self::C),
            "D" => Ok(Self::D),
            _ => Err(format!("Invalid type: {}", s))
        }
    }
}

impl Display for AmphipodTypes {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            AmphipodTypes::A => 'A',
            AmphipodTypes::B => 'B',
            AmphipodTypes::C => 'C',
            AmphipodTypes::D => 'D',
        })
    }
}

const HALL_LENGTH: usize = 11;
const ROOM_COUNT: usize = 4;

#[derive(Clone, Eq, PartialEq, Debug)]
struct BurrowStep {
    pub cost: u64,
    pub current: Burrow,
}

impl PartialOrd<Self> for BurrowStep {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.cost.partial_cmp(&other.cost)
    }
}

impl Ord for BurrowStep {
    fn cmp(&self, other: &Self) -> Ordering {
        self.cost.cmp(&other.cost)
    }
}

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
struct Burrow {
    // #############
    // #...........# <- Hall positions
    // ###.#.#.#.###
    //   #.#.#.#.#   <- Side rooms
    //   #########
    hall: [Option<AmphipodTypes>; HALL_LENGTH],
    rooms: [SmallVec<[Option<AmphipodTypes>; 4]>; ROOM_COUNT],
}

impl Display for Burrow {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "# # # # # # # # # # # # # ")?;
        write!(f, "# ")?;
        for h in 0..HALL_LENGTH {
            if let Some(s) = self.hall[h] {
                write!(f, "{} ", s)?;
            } else {
                write!(f, ". ")?;
            }
        }
        writeln!(f, "# ")?;

        write!(f, "# # # ")?;
        for r in 0..ROOM_COUNT {
            if let Some(s) = self.rooms[r][0] {
                write!(f, "{} # ", s)?;
            } else {
                write!(f, ". # ")?;
            }
        }
        writeln!(f, "# # ")?;

        for d in 0..self.rooms[0].len() {
            write!(f, "    # ")?;
            for r in 0..ROOM_COUNT {
                if let Some(s) = self.rooms[r][d] {
                    write!(f, "{} # ", s)?;
                } else {
                    write!(f, ". # ")?;
                }
            }
            writeln!(f, "")?;
        }

        writeln!(f, "    # # # # # # # # # ")
    }
}

enum BurrowIndex {
    // Left to right
    Hall(usize),
    // Room index, depth into room
    Room(usize, usize)
}

impl Burrow {
    const ROOM_ENTRANCES: [usize; ROOM_COUNT] = [2, 4, 6, 8];

    pub fn load(id: usize) -> Burrow {
        let reader = BufReader::new(File::open(format!("day23/input_{}.txt", id)).unwrap());
        let mut lines = reader.lines().skip(2);

        let mut rooms = std::iter::repeat_with(|| SmallVec::new()).collect_n::<ROOM_COUNT>().unwrap();

        for l in lines.map(|l| l.unwrap()).filter(|l| !l.is_empty()) {
            for (i, t) in l.trim().split('#').filter(|s| !s.is_empty()).map(|s| s.parse::<AmphipodTypes>().unwrap()).enumerate() {
                rooms[i].push(Some(t));
            }
        }

        Burrow {
            hall: [None; HALL_LENGTH],
            rooms
        }
    }

    pub fn is_final(&self) -> bool {
        (0..ROOM_COUNT).flat_map(|r| (0..self.rooms[r].len()).map(move |d| (r, d))).all(|(r, d)| {
            self.rooms[r][d].is_some() && self.rooms[r][d].unwrap() as usize - 1 == r
        })
    }

    // Vec<(cost, new_state)>
    pub fn enumerate_possible_steps(&self) -> Vec<(u64, Burrow)> {
        let hall_options = (0..HALL_LENGTH).filter_map(|h| self.hall[h].map(|s| (h, s))).flat_map(|(h, s)| {
            self.possible_movement_from(BurrowIndex::Hall(h)).into_iter().map(move |(dist, dest)| {
                let mut new = self.clone();
                new.hall[h] = None;
                match dest {
                    BurrowIndex::Hall(h) => new.hall[h] = Some(s),
                    BurrowIndex::Room(r, d) => new.rooms[r][d] = Some(s)
                }

                (dist * s.movement_cost(), new)
            })
        });

        let room_options = (0..ROOM_COUNT).flat_map(|r| (0..self.rooms[r].len()).map(move |d| (r, d))).filter_map(|(r, d)| self.rooms[r][d].map(|s| (r, d, s))).flat_map(|(r, d, s)| {
            self.possible_movement_from(BurrowIndex::Room(r, d)).into_iter().map(move |(dist, dest)| {
                let mut new = self.clone();
                new.rooms[r][d] = None;
                match dest {
                    BurrowIndex::Hall(h) => new.hall[h] = Some(s),
                    BurrowIndex::Room(r, d) => new.rooms[r][d] = Some(s)
                }

                (dist * s.movement_cost(), new)
            })
        });

        hall_options.chain(room_options).collect()
    }

    // Vec<(distance, new_pos)>
    fn possible_movement_from(&self, pos: BurrowIndex) -> Vec<(u64, BurrowIndex)> {
        match pos {
            BurrowIndex::Hall(h) => {
                let self_type = self.hall[h].unwrap();
                // A=0, B=1, C=2, D=3
                let self_type_index = self_type as usize - 1;

                let room_available = self.rooms[self_type_index][0].is_none()
                    && self.rooms[self_type_index].iter().skip(1).all(|v| v.is_none() || v.unwrap() == self_type);

                if !room_available {
                    return vec![];
                }

                // Technically there could be two options, but never a good reason to block entrance
                let available_room = self.rooms[self_type_index].iter().enumerate().filter(|(_, v)| v.is_none()).last().unwrap().0;

                let entrance_hall_index = Self::ROOM_ENTRANCES[self_type_index];
                let range = if entrance_hall_index < h {
                    entrance_hall_index..=(h-1)
                } else {
                    (h+1)..=entrance_hall_index
                };

                let path_to_entrance_clear = range.into_iter().all(|i| self.hall[i].is_none());

                if !path_to_entrance_clear {
                    return vec![];
                }

                let path_length = entrance_hall_index.max(h) - entrance_hall_index.min(h) + available_room + 1;

                vec![(path_length as u64, BurrowIndex::Room(self_type_index, available_room))]
            }
            BurrowIndex::Room(r, d) => {
                if self.rooms[r][0..d].iter().any(|s| s.is_some()) {
                    return vec![];
                }

                // Note: By movement rules, this slot will always be open
                let hall_entrance_index = Self::ROOM_ENTRANCES[r];

                let right_most_hall = (hall_entrance_index..HALL_LENGTH).take_while(|i| self.hall[*i].is_none()).last().unwrap_or(hall_entrance_index);
                let left_most_hall = (0..hall_entrance_index).rev().take_while(|i| self.hall[*i].is_none()).last().unwrap_or(hall_entrance_index);

                let right_outcomes = (hall_entrance_index+1..=right_most_hall).filter(|i| !Self::ROOM_ENTRANCES.contains(i)).map(|i| {
                    ((i - hall_entrance_index + d + 1) as u64, BurrowIndex::Hall(i))
                });

                let left_outcomes = (left_most_hall..hall_entrance_index).filter(|i| !Self::ROOM_ENTRANCES.contains(i)).map(|i| {
                    ((hall_entrance_index - i + d + 1) as u64, BurrowIndex::Hall(i))
                });


                let self_type = self.rooms[r][d].unwrap();
                // A=0, B=1, C=2, D=3
                let self_type_index = self_type as usize - 1;
                let target_room_entrance = Self::ROOM_ENTRANCES[self_type_index];

                let target_room_outcome = ((left_most_hall..=right_most_hall).contains(&target_room_entrance) && // Can reach target room entrance
                    self.rooms[self_type_index][0].is_none() && self.rooms[self_type_index].iter().skip(1).all(|v| v.is_none() || v.unwrap() == self_type)).then(|| {// Target room has room, or only contain self type

                    let available_room = self.rooms[self_type_index].iter().enumerate().filter(|(_, v)| v.is_none()).last().unwrap().0;

                    let path_length = target_room_entrance.max(hall_entrance_index) - target_room_entrance.min(hall_entrance_index) + available_room + 1 + d + 1;

                    (path_length as u64, BurrowIndex::Room(self_type_index, available_room))
                });

                left_outcomes.chain(right_outcomes).chain(target_room_outcome).collect()
            }
        }
    }
}
