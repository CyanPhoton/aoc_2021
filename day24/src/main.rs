use std::collections::{HashMap, HashSet};
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::{AddAssign, DivAssign, MulAssign, RangeInclusive, RemAssign};
use std::str::FromStr;
use common::utility;

///
/// NOTE:
///
/// These values where found via manually analysing the "code" to find a general method
///
/// This result is that there are 3 arrays of 14 values:
/// ```
/// let Δ = [1,  1,  1,1 ,   26,  1,  26,  1, 26, 26, 26,  26,  1,  26]; // (from lines 5 +  18 * k)
/// let Ω = [14, 11, 11, 14, -11, 12, -1, 10, -3, -4, -13, -8, 13, -11]; // (from lines 6 +  18 * k)
/// let Θ = [12, 8,  7,  4,   4,   1, 10,  8, 12, 10,  15,  4, 10,   9]; // (from lines 16 + 18 * k)
/// ```
///
/// There is also the array of input values, I denote `ω[n]`
/// The output values are the z register, which I consider as an array `z[n]` for each step
///
/// With which I also define:
/// ```
/// let α = ω.zip(Θ).map(|(ω_n, Θ_n)| ω_n + Θ_n);
/// let β = ω.zip(Ω).map(|(ω_n, Ω_n)| ω_n - Ω_n);
/// ```
///
/// Then the function is (not / is c style integer divide, rounds down)
/// (Note assumes that Δ[n] is either 1 or 26)
///
/// ```
/// z[n] = if Δ[n] == 1 {
///     // Branch A
///     z[n-1] * 26 + ω[n] + α[n]
/// } else if β[n] == z[n-1] % 26 {
///     // Branch B
///     z[n-1] / 26
/// } else {
///     // Branch C
///     (z[n-1] / 26) * 26 + α[n] // Note: div and mul don't cancel since integer division
/// }
/// ```
///
/// Then by analysing that branch A happens 7 times, so if z[14] is to be 0,
/// branch B must occur the other 7 times, hence C never happens, so we can change to:
///
/// z[n] = if Δ[n] == 1 {
///     // Branch A
///     z[n-1] * 26 + ω[n] + α[n]
/// } else {
///     // Branch B
///     assert_eq!(β[n], z[n-1] % 26);
///     z[n-1] / 26
/// }
/// ```
///
/// Then we manually perform this calculation, confirm that z[14] == 0, and collect our asserts
/// (evaluating the %)
///
/// α[4]  == β[5]
/// α[6]  == β[7]
/// α[8]  == β[9]
/// α[3]  == β[10]
/// α[2]  == β[11]
/// α[1]  == β[12]
/// α[13] == β[14]
///
/// This in turn gives, 7 pairs of relations between some ω values,
/// then just pick the values in each pair that either maximise ot minimise the pair
/// this is easy since no pairs overlap
///
fn main() {
    let instruction = read_instruction(|v| v);

    {
        // Highest = 59692994994998
        let test_highest = [5, 9, 6, 9, 2, 9, 9, 4, 9, 9, 4, 9, 9, 8];
        let mut ptr = 0;

        let mut alu = ALU::new();
        for i in &instruction[0..] {
            alu.process_instruction(i, || {
                let v = test_highest[ptr];
                ptr += 1;
                v
            });
        }

        println!("P1: eval(59692994994998).z = {}", alu.state_vector[3]);
    }

    {
        // Lowest = 16181111641521
        let test_lowest = [1, 6, 1, 8, 1, 1, 1, 1, 6, 4, 1, 5, 2, 1];
        let mut ptr = 0;

        let mut alu = ALU::new();
        for i in &instruction[0..] {
            alu.process_instruction(i, || {
                let v = test_lowest[ptr];
                ptr += 1;
                v
            });
        }

        println!("P2: eval(16181111641521).z = {}", alu.state_vector[3]);
    }
}

pub trait CompareAssign {
    fn compare_assign(&mut self, other: Self);
}

impl CompareAssign for i64 {
    fn compare_assign(&mut self, other: Self) {
        *self = if *self == other { 1 } else { 0 };
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Quantity {
    Concrete(i64),
    Symbolic(String, RangeInclusive<i64>),
}

impl Display for Quantity {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Quantity::Concrete(b) => write!(f, "{}", b),
            Quantity::Symbolic(s, _) => write!(f, "{}", s)
        }
    }
}

#[derive(Debug)]
pub struct ALU<T> {
    state_vector: [T; REGISTER_COUNT],
}

impl<T> ALU<T> {
    pub fn new() -> ALU<T> where T: Default + Copy {
        ALU {
            state_vector: [T::default(); REGISTER_COUNT]
        }
    }

    pub fn init(state_vector: [T; REGISTER_COUNT]) -> ALU<T> {
        ALU {
            state_vector
        }
    }

    pub fn process_instruction<F: FnOnce() -> T>(&mut self, instruction: &Instruction<T>, input: F) where T: Clone + AddAssign + MulAssign + DivAssign + RemAssign + CompareAssign {
        let get_b = |this: &ALU<T>, b: &Value<T>| {
            match b {
                Value::Literal(i) => i.clone(),
                Value::Register(r) => this.state_vector[*r as usize].clone()
            }
        };

        match instruction {
            Instruction::Input(a) => self.state_vector[*a as usize] = input(),
            Instruction::Add(a, b) => self.state_vector[*a as usize] += get_b(self, b),
            Instruction::Mul(a, b) => self.state_vector[*a as usize] *= get_b(self, b),
            Instruction::Div(a, b) => self.state_vector[*a as usize] /= get_b(self, b),
            Instruction::Mod(a, b) => self.state_vector[*a as usize] %= get_b(self, b),
            Instruction::Eq(a, b) => self.state_vector[*a as usize].compare_assign(get_b(self, b)),
        }
    }
}

const REGISTER_COUNT: usize = 4;

#[derive(Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Debug, Hash)]
pub enum Register {
    W = 0,
    X = 1,
    Y = 2,
    Z = 3,
}

impl FromStr for Register {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "w" => Ok(Register::W),
            "x" => Ok(Register::X),
            "y" => Ok(Register::Y),
            "z" => Ok(Register::Z),
            _ => Err(format!("Invalid input: {}", s))
        }
    }
}

pub enum Value<T> {
    Literal(T),
    Register(Register),
}

impl<T> FromStr for Value<T> where T: FromStr {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse::<Register>().map(|r| Value::Register(r))
            .or_else(|_| s.parse::<T>().map_err(|_| format!("Invalid input: {}", s))
                .map(|v| Value::Literal(v))
            )
    }
}

pub enum Instruction<T> {
    Input(Register),
    Add(Register, Value<T>),
    Mul(Register, Value<T>),
    Div(Register, Value<T>),
    Mod(Register, Value<T>),
    Eq(Register, Value<T>),
}

impl<T> FromStr for Instruction<T> where T: FromStr {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut sections = s.split(' ').filter(|s| !s.is_empty());

        match sections.next().unwrap() {
            "inp" => Ok(Instruction::Input(sections.next().unwrap().parse()?)),
            "add" => Ok(Instruction::Add(sections.next().unwrap().parse()?, sections.next().unwrap().parse()?)),
            "mul" => Ok(Instruction::Mul(sections.next().unwrap().parse()?, sections.next().unwrap().parse()?)),
            "div" => Ok(Instruction::Div(sections.next().unwrap().parse()?, sections.next().unwrap().parse()?)),
            "mod" => Ok(Instruction::Mod(sections.next().unwrap().parse()?, sections.next().unwrap().parse()?)),
            "eql" => Ok(Instruction::Eq(sections.next().unwrap().parse()?, sections.next().unwrap().parse()?)),
            _ => Err(format!("Invalid input: {}", s))
        }
    }
}

pub fn read_instruction<T, F: FnMut(i64) -> T>(mut f: F) -> Vec<Instruction<T>> {
    let reader = BufReader::new(File::open("day24/input.txt").unwrap());

    let mut g = |v: Value<i64>| {
        match v {
            Value::Literal(v) => Value::Literal(f(v)),
            Value::Register(r) => Value::Register(r)
        }
    };

    reader.lines().map(|l| l.unwrap()).filter(|l| !l.is_empty()).map(|l| match l.parse::<Instruction<i64>>().unwrap() {
        Instruction::Input(i) => Instruction::Input(i),
        Instruction::Add(a, b) => Instruction::Add(a, g(b)),
        Instruction::Mul(a, b) => Instruction::Mul(a, g(b)),
        Instruction::Div(a, b) => Instruction::Div(a, g(b)),
        Instruction::Mod(a, b) => Instruction::Mod(a, g(b)),
        Instruction::Eq(a, b) => Instruction::Eq(a, g(b)),
    }).collect()
}