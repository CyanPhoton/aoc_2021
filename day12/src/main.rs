use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::AddAssign;
use fxhash::FxHashMap;

fn main() {
    problem_1();
    problem_2();
}

#[derive(Debug)]
struct Node {
    #[allow(dead_code)]
    name: String,
    is_big_cave: bool,
    adjacent: Vec<String>
}

fn build_graph() -> FxHashMap<String, Node> {
    let reader = BufReader::new(File::open("day12/input.txt").unwrap());

    let mut graph: HashMap<String, Node, _> = Default::default();

    for line in reader.lines() {
        let line = line.unwrap();
        let (a, b) = line.split_once('-').unwrap();
        let (a, b) = (a.to_string(), b.to_string());

        graph.entry(a.clone()).or_insert(Node {
            name: a.clone(),
            is_big_cave: a.chars().all(|c| c.is_uppercase()),
            adjacent: vec![]
        }).adjacent.push(b.clone());

        graph.entry(b.clone()).or_insert(Node {
            is_big_cave: b.chars().all(|c| c.is_uppercase()),
            name: b,
            adjacent: vec![]
        }).adjacent.push(a);
    }

    for (_, node) in &mut graph {
        node.adjacent.sort();
    }

    graph
}


fn problem_1() {
    let graph = build_graph();

    let mut path_stack = Vec::<(String, usize)>::new();
    path_stack.push(("start".to_string(), 0));

    let mut paths = Vec::new();

    while !path_stack.is_empty() {
        let top = path_stack.last().unwrap();

        if &top.0 == "end" {
            paths.push(path_stack.iter().map(|p| p.0.clone()).collect::<Vec<_>>().join(","));
            path_stack.pop();
        } else if let Some(next_name) = graph[&top.0].adjacent.get(top.1).cloned() {
            path_stack.last_mut().unwrap().1.add_assign(1);

            let next = &graph[&next_name];
            if next.is_big_cave || !path_stack.iter().any(|p| &p.0 == &next_name) {
                path_stack.push((next_name, 0));
            }
        } else {
            path_stack.pop();
        }
    }

    println!("P1: Number of paths = {}", paths.len());
    // dbg!(paths);
}

fn problem_2() {
    let graph = build_graph();

    // (Node name, next adj to take, if path up to this point has doubled up a single small cave)
    let mut path_stack = Vec::<(&str, usize, bool)>::new();
    path_stack.push(("start", 0, false));

    let mut paths = Vec::new();

    while !path_stack.is_empty() {
        let top = path_stack.last().unwrap();

        if top.0 == "end" {
            paths.push(path_stack.iter().map(|p| p.0.clone()).collect::<Vec<_>>().join(","));
            path_stack.pop();
        } else if let Some(next_name) = graph[top.0].adjacent.get(top.1) {
            let top_used_double = top.2;
            path_stack.last_mut().unwrap().1.add_assign(1);

            let next = &graph[next_name];
            let node_unique = !path_stack.iter().any(|p| &p.0 == next_name);
            if (next.is_big_cave || node_unique || !top_used_double) && next_name != "start" {
                path_stack.push((next_name.as_str(), 0, top_used_double || (!node_unique && !next.is_big_cave)));
            }
        } else {
            path_stack.pop();
        }
    }

    println!("P2: Number of paths = {}", paths.len());
    // dbg!(paths);
}
