use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    problem_1();
    problem_2();
}

const BITS_PER_LINE: usize = 12;

fn problem_1() {
    let reader = BufReader::new(File::open("day3/input.txt").unwrap());

    let mut net_frequency = [0i64; BITS_PER_LINE];

    for line in reader.lines() {
        let line = line.unwrap();
        let line_num = u16::from_str_radix(&line, 2).unwrap();
        for i in 0..BITS_PER_LINE {
            net_frequency[i] += (((line_num >> i) & 1) as i64) * 2 - 1;
        }
    }

    let mut gamma_rate = 0u32;
    for i in (0..BITS_PER_LINE).rev() {
        gamma_rate <<= 1;
        if net_frequency[i] > 0 {
            gamma_rate += 1;
        } else if net_frequency[i] == 0 {
            panic!("Neither is more common");
        }
    }

    let epsilon_rate = (!gamma_rate) & ((1 << BITS_PER_LINE) - 1);

    println!("P1: gamma: {0} ({0:#014b}), epsilon: {1} ({1:#014b}), product: {2}", gamma_rate, epsilon_rate, gamma_rate * epsilon_rate);
}

fn problem_2() {
    let reader = BufReader::new(File::open("day3/input.txt").unwrap());

    let mut o2_gen_options = Vec::new();
    let mut co2_scrub_options = Vec::new();

    for line in reader.lines() {
        let line = line.unwrap();
        let line_num = u16::from_str_radix(&line, 2).unwrap();

        o2_gen_options.push(line_num);
    }
    co2_scrub_options = o2_gen_options.clone();

    let mut last_o2 = None;
    for i in (0..BITS_PER_LINE).rev() {
        let mut frequency = 0;
        for o in &o2_gen_options {
            frequency += (((*o >> i) & 1) as i64) * 2 - 1;
        }

        let o2_target = if frequency >= 0 { 1 } else { 0 };

        o2_gen_options.retain(|option| {
            last_o2 = Some(*option);
            (*option >> i) & 1 == o2_target
        });

        if o2_gen_options.is_empty() {
            break;
        }
    }

    let mut last_co2 = None;
    for i in (0..BITS_PER_LINE).rev() {
        let mut frequency = 0;
        for o in &co2_scrub_options {
            frequency += (((*o >> i) & 1) as i64) * 2 - 1;
        }

        let co2_target = if frequency >= 0 { 0 } else { 1 };

        co2_scrub_options.retain(|option| {
            last_co2 = Some(*option);
            (*option >> i) & 1 == co2_target
        });

        if co2_scrub_options.is_empty() {
            break;
        }
    }

    println!("P2: O2: {}, CO2: {}, product: {}", last_o2.unwrap(), last_co2.unwrap(), last_o2.unwrap() as u32 * last_co2.unwrap() as u32);
}