use std::io::{BufRead, BufReader};

fn main() {
    problem_1();
    problem_2();
}

fn problem_1() {
    let mut reader = BufReader::new(std::fs::File::open("day1/input.txt").unwrap());

    let mut increase_count = 0;
    let mut prev = None;
    for line in reader.lines() {
        let line = line.unwrap();
        let value = line.parse::<u32>().unwrap();

        if let Some(prev) = prev {
            if value > prev {
                increase_count += 1;
            }
        }

        prev = Some(value);
    }

    println!("P1: Total increases: {}", increase_count);
}

fn problem_2() {
    let mut reader = BufReader::new(std::fs::File::open("day1/input.txt").unwrap());
    let mut lines = reader.lines();
    let mut increase_count = 0;

    let mut prev_sum = None;
    let mut window = [
        lines.next().unwrap().unwrap().parse::<u32>().unwrap(),
        lines.next().unwrap().unwrap().parse::<u32>().unwrap(),
    ];

    while let Some(line) = lines.next() {
        let line = line.unwrap();
        let value = line.parse::<u32>().unwrap();

        let sum = value + window.into_iter().sum::<u32>();

        if let Some(prev_sum) = prev_sum {
            if sum > prev_sum {
                increase_count += 1;
            }
        }

        window[0] = window[1];
        window[1] = value;

        prev_sum = Some(sum);
    }

    println!("P2: Total increases: {}", increase_count);
}
