use std::fs::{File, read};
use std::io::{BufRead, BufReader};

fn main() {
    let reader = BufReader::new(File::open("day17/input.txt").unwrap());
    let line = reader.lines().next().unwrap().unwrap();

    let (x_range, y_range) = line.trim_start_matches("target area:").trim().split_once(',').unwrap();
    let (x_range, y_range) = (x_range.trim_start_matches("x=").trim(), y_range.trim().trim_start_matches("y="));
    let (x_range, y_range) = (x_range.split_once("..").unwrap(), y_range.split_once("..").unwrap());

    let (x_min, x_max) = (x_range.0.parse::<i64>().unwrap(), x_range.1.parse::<i64>().unwrap());
    let (y_min, y_max) = (y_range.0.parse::<i64>().unwrap(), y_range.1.parse::<i64>().unwrap());

    let mut max_uy = None;
    for uy in 1.. {
        for t in 1.. {
            let y = uy * t - (t*t - t) / 2;

            if y_min <= y && y <= y_max {
                max_uy = Some(uy);
            } else if y < y_min {
                break;
            }
        }
        if max_uy.is_some() && max_uy.unwrap() < uy/2 {
            break;
        }
    }
    let max_uy = max_uy.unwrap();
    let max_y = (max_uy * max_uy + max_uy) / 2;

    println!("P1: {}", max_y);

    let mut possible = Vec::new();
    for uy in -max_uy*2..=max_uy*2 {
        'ux: for ux in 1..=x_max {
            for t in 1.. {
                let y = uy * t - (t * t - t) / 2;
                let x = if t <= ux {
                    ux * t - (t * t - t) / 2
                } else {
                    (ux * ux + ux) / 2
                };

                if y_min <= y && y <= y_max && x_min <= x && x <= x_max {
                    possible.push((ux, uy));
                    break;
                } else if y < y_min || x > x_max {
                    break;
                }
            }
        }
    }

    println!("P2: {}", possible.len());
    // dbg!(&possible);
}
