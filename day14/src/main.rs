use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    problem_1();
    problem_2();
}

pub struct Polymer {
    pairs: [u128; ALPHABET_SIZE * ALPHABET_SIZE],
    rules: [Option<(u16, u16)>; ALPHABET_SIZE * ALPHABET_SIZE],
    last_char: char
}

const ALPHABET_SIZE: usize = 'Z' as usize - 'A' as usize + 1;

impl Polymer {
    pub fn load() -> Polymer {
        let reader = BufReader::new(File::open("day14/input.txt").unwrap());
        let mut lines = reader.lines();

        let (units, last_char) = {
            let mut units = [0u128; ALPHABET_SIZE * ALPHABET_SIZE];

            let template = lines.next().unwrap().unwrap();
            for pair in template.chars().zip(template.chars().skip(1)) {
                units[Self::pair_to_index(pair)] += 1;
            }

            let last_char = template.chars().last().unwrap();

            (units, last_char)
        };

        let rules = {
            let mut rules = [None; ALPHABET_SIZE * ALPHABET_SIZE];

            for rule in lines.filter(|l| !l.as_ref().unwrap().is_empty()) {
                let rule = rule.unwrap();
                let (rule, insert) = rule.split_once(" -> ").unwrap();
                let mut rule = rule.chars();
                let mut insert = insert.chars();

                let left = rule.next().unwrap();
                let middle = insert.next().unwrap();
                let right = rule.next().unwrap();

                let rule_index = Self::pair_to_index((left, right));
                let new_left_pair = Self::pair_to_index((left, middle)) as u16;
                let new_right_pair = Self::pair_to_index((middle, right)) as u16;

                rules[rule_index] = Some((new_left_pair, new_right_pair));
            }

            rules
        };

        Polymer {
            pairs: units,
            rules,
            last_char
        }
    }

    pub fn step(&mut self) {
        let mut new_units = [0u128; ALPHABET_SIZE * ALPHABET_SIZE];

        for i in 0..(ALPHABET_SIZE * ALPHABET_SIZE) {
            if let Some(rule) = self.rules[i] {
                new_units[rule.0 as usize] += self.pairs[i];
                new_units[rule.1 as usize] += self.pairs[i];
            }
        }

        self.pairs = new_units;
    }

    fn pair_to_index(pair: (char, char)) -> usize {
        ((pair.0 as usize - 'A' as usize) * ALPHABET_SIZE) + (pair.1 as usize - 'A' as usize)
    }

    fn index_to_pair(index: usize) -> (char, char) {
        let l = ((index / ALPHABET_SIZE) as u8 + 'A' as u8) as char;
        let r = ((index % ALPHABET_SIZE) as u8 + 'A' as u8) as char;

        (l, r)
    }

    pub fn calc_frequencies(&self) -> [u128; ALPHABET_SIZE] {
        let mut frequencies = [0u128; ALPHABET_SIZE];

        for i in 0..(ALPHABET_SIZE * ALPHABET_SIZE) {
            let (l, r) = Self::index_to_pair(i);

            frequencies[l as usize - 'A' as usize] += self.pairs[i];
            // frequencies[r as usize - 'A' as usize] += self.pairs[i];
        }

        frequencies[self.last_char as usize - 'A' as usize] += 1;

        frequencies
    }

    pub fn calc_min_max_diff(&self) -> u128 {
        let frequencies = self.calc_frequencies();

        let max = frequencies.iter().copied().max().unwrap();
        let min = frequencies.iter().copied().filter(|f| *f > 0).min().unwrap_or(0);

        max - min
    }
}

fn problem_1() {
    let mut polymer = Polymer::load();

    for _ in 0..10 {
        polymer.step();
    }

    println!("P1: Diff = {}", polymer.calc_min_max_diff());
}

fn problem_2() {
    let mut polymer = Polymer::load();

    for _ in 0..40 {
        polymer.step();
    }

    println!("P1: Diff = {}", polymer.calc_min_max_diff());
}
