use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    problem_1();
    problem_2();
}

fn problem_1() {
    let reader = BufReader::new(File::open("day2/input.txt").unwrap());

    let mut x = 0;
    let mut y = 0;

    for line in reader.lines() {
        let line = line.unwrap();

        let (dir, dist) = line.split_once(' ').unwrap();
        let dist = dist.parse::<i32>().unwrap();

        match dir {
            "forward" => x += dist,
            "down" => y -= dist,
            "up" => y += dist,
            _ => panic!("Invalid dir: {}", dir)
        }
    }

    println!("P1: Horizontal: {}, Depth: {} -> product = {}", x, -y, x * (-y));
}

fn problem_2() {
    let reader = BufReader::new(File::open("day2/input.txt").unwrap());

    let mut x = 0;
    let mut y = 0;
    let mut gradient = 0;

    for line in reader.lines() {
        let line = line.unwrap();

        let (dir, dist) = line.split_once(' ').unwrap();
        let dist = dist.parse::<i32>().unwrap();

        match dir {
            "forward" => {
                x += dist;
                y += gradient * dist;
            },
            "down" => gradient -= dist,
            "up" => gradient += dist,
            _ => panic!("Invalid dir: {}", dir)
        }
    }

    println!("P2: Horizontal: {}, Depth: {} -> product = {}", x, -y, x * (-y));
}

