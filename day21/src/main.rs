use std::collections::{HashMap, HashSet};
use std::fs::{File, read};
use std::io::{BufRead, BufReader};
use std::ops::AddAssign;
use common::Combinations;

fn main() {
    problem_1();
    problem_2();
    // test();
}

fn problem_1() {
    let players = load_players();

    let mut die = DeterministicDice::new(100);
    let mut board = Board::new(10, 3, 1000, players);

    loop {
        match board.play_turn(&mut die) {
            None => {}
            Some(p) => {
                let id = p.id;
                let p1 = board.players.iter().find(|o| o.id != id).unwrap().score as usize * die.roll_count;
                println!("P1: {}", p1);
                break;
            }
        }
    }
}

// fn test() {
//     let mut wins = HashMap::<usize, usize>::new();
//
//     let mut used_combo = HashSet::<Vec<u8>>::new();
//
//     for combo in [1,2,3].into_iter().combinations::<8>() {
//         let mut die = FixedSequenceDice::new(combo, 3);
//         let players = load_players();
//         let mut board = Board::new(10, 1, 10, players);
//
//         if combo == [1,1,1,3,1,1,1,1] {
//             dbg!()
//         }
//
//         loop {
//             match board.play_turn(&mut die) {
//                 None => {}
//                 Some(p) => {
//                     let trunc_combo = combo[0..die.roll_count].iter().copied().collect::<Vec<_>>();
//                     if used_combo.insert(trunc_combo.clone()) {
//                         println!("{} WINS with: {:?}", p.id, trunc_combo);
//                         wins.entry(p.id).or_default().add_assign(1);
//                     }
//                     break;
//                 }
//             }
//         }
//     }
//
//     dbg!(&wins);
// }

fn problem_2() {
    let players = load_players();

    const WIN: usize = 21;
    // turn -> ( player -> score )
    let mut wins = HashMap::<usize, HashMap<usize, usize>>::new();

    let mut turn_info = HashMap::new();


    for player in &players {
        let mut turns = vec![{
            let mut per_score = HashMap::new();
            let mut pos_count = HashMap::new();
            pos_count.insert(player.position, 1);
            per_score.insert(0, pos_count);

            TurnData {
                per_score
            }
        }];

        let mut all_rolls = [1, 2, 3].into_iter().combinations::<3>().collect::<Vec<_>>();

        while !turns.last().unwrap().per_score.is_empty() {

            let last = turns.last().unwrap();

            let mut new_turn = TurnData { per_score: Default::default() };

            for (score, (pos, count)) in last.per_score.iter().flat_map(|(k, v)| v.iter().map(move |v| (k, v))) {
                if *score < WIN {
                    for roll in &all_rolls {
                        let roll_sum: u8 = roll.iter().copied().sum();

                        let new_pos = (*pos + roll_sum - 1) % 10 + 1;
                        let new_score = *score + new_pos as usize;

                        new_turn.per_score.entry(new_score).or_default().entry(new_pos).or_default().add_assign(*count);

                        if new_score >= WIN {
                            wins.entry(turns.len()).or_default().entry(player.id).or_default().add_assign(*count);
                        }
                    }
                }
            }

            turns.push(new_turn);
        }
        // dbg!(&turns);
        turn_info.insert(player.id, turns);
    }
    // dbg!(&turn_info);
    // dbg!(&wins);

    let mut total_wins = HashMap::<usize, usize>::new();


    for (turn, wins_per_player) in wins {
        for (player, wins) in wins_per_player {
            let multi = {
                let pre_product = players.iter()
                    .filter(|p| p.id < player)
                    .map(|p|
                        turn_info[&p.id][turn].per_score.iter()
                            .filter(|(s, p)| **s < WIN)
                            .map(|(_, p)|
                                p.iter()
                                    .map(|v| *v.1)
                                    .sum::<usize>()
                            )
                            .sum::<usize>()
                    )
                    .product::<usize>();

                let post_product = players.iter()
                    .filter(|p| p.id > player)
                    .map(|p|
                        turn_info[&p.id][turn - 1].per_score.iter()
                            .filter(|(s, p)| **s < WIN)
                            .map(|(_, p)|
                                p.iter()
                                    .map(|v| *v.1)
                                    .sum::<usize>()
                            )
                            .sum::<usize>()
                    )
                    .product::<usize>();

                pre_product * post_product
            };

            total_wins.entry(player).or_default().add_assign(wins * multi);
        }
    }

    dbg!(&total_wins);

    // println!("P2: {} won in {}", p2.0, p2.1);
}

#[derive(Debug)]
struct TurnData {
    // Score -> (pos -> count)
    per_score: HashMap<usize, HashMap<u8, usize>>
}

#[derive(Clone)]
struct Board {
    size: u8,
    roll_count: u8,
    winning_score: u32,
    players: Vec<Player>,
    player_ptr: usize,
    won: bool
}

impl Board {
    pub fn new(size: u8, roll_count: u8, winning_score: u32, players: Vec<Player>) -> Self {
        Board { size, roll_count, winning_score, players, player_ptr: 0, won: false }
    }

    pub fn play_turn<D: Dice>(&mut self, die: &mut D) -> Option<&Player> {
        if self.won {
            return Some(&self.players[self.player_ptr]);
        }

        let roll = (0..self.roll_count).map(|_| die.roll() as u32).sum::<u32>();
        let roll = (roll % self.size as u32) as u8;

        self.players[self.player_ptr].position = ((self.players[self.player_ptr].position - 1 + roll) % self.size) + 1;
        self.players[self.player_ptr].score += self.players[self.player_ptr].position as u32;
        (self.players[self.player_ptr].score >= self.winning_score)
            .then(|| {
                self.won = true;
                &self.players[self.player_ptr]
            })
            .or_else(|| {
            self.player_ptr += 1;
            self.player_ptr %= self.players.len();
            None
        })
    }
}

#[derive(Copy, Clone)]
struct Player {
    id: usize,
    position: u8,
    score: u32
}

fn load_players() -> Vec<Player> {
    let reader = BufReader::new(File::open("day21/input.txt").unwrap());

    reader.lines().map(|l| l.unwrap()).filter(|l| !l.is_empty()).map(|l| {
        let (id, position) = l.trim_start_matches("Player ").split_once(" starting position: ").unwrap();
        let (id, position) = (id.parse::<usize>().unwrap(), position.parse::<u8>().unwrap());
        Player { id, position, score: 0 }
    }).collect()
}

trait Dice {
    fn roll(&mut self) -> u8;
    fn sides(&self) -> u8;
}

struct DeterministicDice {
    last_value: u8,
    sides: u8,
    roll_count: usize,
}

impl DeterministicDice {
    pub fn new(sides: u8) -> DeterministicDice {
        DeterministicDice {
            last_value: 0,
            sides,
            roll_count: 0
        }
    }
}

impl Dice for DeterministicDice {
    fn roll(&mut self) -> u8 {
        self.roll_count += 1;
        self.last_value = (self.last_value % self.sides) + 1;
        self.last_value
    }

    fn sides(&self) -> u8 {
        self.sides
    }
}

struct FixedSequenceDice<const N: usize> {
    values: [u8; N],
    ptr: usize,
    sides: u8,
    roll_count: usize,
}

impl<const N: usize> FixedSequenceDice<N> {
    pub fn new(values: [u8; N], sides: u8) -> Self {
        FixedSequenceDice { values, ptr: 0, sides, roll_count: 0 }
    }
}

impl<const N: usize> Dice for FixedSequenceDice<N> {
    fn roll(&mut self) -> u8 {
        let r = self.values[self.ptr];
        self.ptr += 1;
        self.ptr %= self.values.len();
        self.roll_count += 1;
        r
    }

    fn sides(&self) -> u8 {
        self.sides
    }
}
