use std::convert::identity;
use std::fs::{File, read};
use std::io::{BufRead, BufReader};
use std::ops::RangeInclusive;
use indicatif::ProgressBar;
use common::{CollectN, Combinations};

fn main() {
    let actions = load_actions();
    let mut reactor = Reactor::new();

    let pb = ProgressBar::new(actions.len() as u64);
    for action in &actions {
        reactor.perform_action(action);
        pb.inc(1);
    }
    pb.finish_with_message("Done applying");
    let boot_region = Region::new((-50,-50,-50),(50,50,50));
    println!("P1: {}", reactor.count_on_fully_inside(&boot_region));
    println!("P2: {}", reactor.count_on());
}

fn load_actions() -> Vec<Action> {
    let reader = BufReader::new(File::open("day22/input.txt").unwrap());

    reader.lines().map(|l| l.unwrap()).filter(|l| !l.is_empty()).map(|l| {
        let (action, region) = l.split_once(' ').unwrap();
        let action = action == "on";

        let [x, y, z]: [_; 3] = region.split(',').map(|r| {
            let (_axis, range) = r.split_once('=').unwrap();
            let (lower, upper) = range.split_once("..").unwrap();
            let lower = lower.parse().unwrap();
            let upper = upper.parse().unwrap();
            lower..=upper
        }).collect_n().unwrap();

        Action {
            action,
            region: Region {
                x,
                y,
                z,
            },
        }
    }).collect()
}

struct Action {
    action: bool,
    region: Region,
}

#[derive(Eq, PartialEq, Debug, Clone)]
struct Region {
    x: RangeInclusive<i32>,
    y: RangeInclusive<i32>,
    z: RangeInclusive<i32>,
}

impl Region {
    pub fn new(p1: (i32, i32, i32), p2: (i32, i32, i32)) -> Region {
        Region {
            x: p1.0.min(p2.0)..=p1.0.max(p2.0),
            y: p1.1.min(p2.1)..=p1.1.max(p2.1),
            z: p1.2.min(p2.2)..=p1.2.max(p2.2)
        }
    }

    pub fn volume(&self) -> u128 {
        (*self.x.end() - self.x.start() + 1) as u128
        * (*self.y.end() - self.y.start() + 1) as u128
        * (*self.z.end() - self.z.start() + 1) as u128
    }

    // Returns, (broken up self, intersection, broken up other)
    pub fn intersect(&self, other: &Region) -> (Vec<Region>, Option<Region>, Vec<Region>) {
        if self == other {
            // If equal, just return the full intersection
            return (vec![], Some(self.clone()), vec![]);
        }

        let edges_indices = Self::edges_indices();

        let self_corners = self.corners();
        let other_corners = other.corners();

        let corners_inside_self = other_corners.map(|c| self.contains(c));
        let corners_inside_other = self_corners.map(|c| other.contains(c));

        let edges_through_self = edges_indices.map(|i| self.contains_line(other_corners[i.0], other_corners[i.1]));
        let edges_through_other = edges_indices.map(|i| other.contains_line(self_corners[i.0], self_corners[i.1]));

        let intersection = if corners_inside_self.into_iter().all(identity) {
           // other is fully inside self
            other.clone()
        } else if corners_inside_other.into_iter().all(identity) {
            // self is fully inside other
            self.clone()
        } else if corners_inside_self.into_iter().any(identity) {
            // At least 1 corner of other in self
            let in_corner = corners_inside_self.into_iter().enumerate().find(|(i, c)| *c).unwrap().0;
            let opposite_corner = Self::opposite_corner(in_corner);

            let in_corner_pos = other_corners[in_corner];
            let opposite_corner_pos = other_corners[opposite_corner];
            let opposite_corner_clamped = self.clamp_point(opposite_corner_pos);

            Region::new(in_corner_pos, opposite_corner_clamped)
        } else if corners_inside_other.into_iter().any(identity) {
            // At least 1 corner of self in other
            let in_corner = corners_inside_other.into_iter().enumerate().find(|(_, c)| *c).unwrap().0;
            let opposite_corner = Self::opposite_corner(in_corner);

            let in_corner_pos = self_corners[in_corner];
            let opposite_corner_pos = self_corners[opposite_corner];
            let opposite_corner_clamped = other.clamp_point(opposite_corner_pos);

            Region::new(in_corner_pos, opposite_corner_clamped)
        } else if edges_through_self.into_iter().any(identity) {
            // At least 1 edge of other through self
            let through_edge = edges_through_self.into_iter().enumerate().find(|(_, c)| *c).unwrap().0;
            let through_edge_start = edges_indices[through_edge].0;
            let through_edge_start_pos = other_corners[through_edge_start];
            let through_edge_start_clamped = self.clamp_point(through_edge_start_pos);

            let opposite_corner = Self::opposite_corner(through_edge_start);
            let opposite_corner_pos = other_corners[opposite_corner];
            let opposite_corner_clamped = self.clamp_point(opposite_corner_pos);

            Region::new(through_edge_start_clamped, opposite_corner_clamped)
        } else if edges_through_other.into_iter().any(identity) {
            // At least 1 edge of self through other
            let through_edge = edges_through_other.into_iter().enumerate().find(|(_, c)| *c).unwrap().0;
            let through_edge_start = edges_indices[through_edge].0;
            let through_edge_start_pos = self_corners[through_edge_start];
            let through_edge_start_clamped = other.clamp_point(through_edge_start_pos);

            let opposite_corner = Self::opposite_corner(through_edge_start);
            let opposite_corner_pos = self_corners[opposite_corner];
            let opposite_corner_clamped = other.clamp_point(opposite_corner_pos);

            Region::new(through_edge_start_clamped, opposite_corner_clamped)
        } else {
            // No intersection
            return (vec![self.clone()], None, vec![other.clone()]);
        };

        let self_split = self.cut_out(&intersection);
        let other_split = other.cut_out(&intersection);

        (self_split, Some(intersection), other_split)
    }

    // Splits into sub regions, excluding sub_region, and any 0 volume ones
    fn cut_out(&self, sub_region: &Region) -> Vec<Region> {
        assert!(sub_region.corners().into_iter().all(|c| self.contains(c)));
        let mut remaining_self = self.clone();
        let mut sub_regions = vec![];

        let values = [*sub_region.x.end(), *sub_region.y.end(), *sub_region.z.end(), *sub_region.x.start(), *sub_region.y.start(), *sub_region.z.start()];

        for i in 0..6 {
            let (rem, split) = remaining_self.split_off(i, values[i]);
            remaining_self = rem;
            sub_regions.extend(split);
        }

        sub_regions
    }

    // Axis, 0=+x,1=+y,2=+z
    //       3=-x,4=-y,5=-z
    //       is split off
    // value, remains inside
    fn split_off(&self, axis_dir: usize, value: i32) -> (Region, Option<Region>) {
        let axis = [&self.x, &self.y, &self.z];
        let border = [*self.x.end(), *self.y.end(), *self.z.end(), *self.x.start(), *self.y.start(), *self.z.start()];

        if axis[axis_dir % 3].contains(&value) && value != border[axis_dir] {
            // let ranges = [*self.x.start()..=value, value+1..=*self.x.end()];
            let r_ranges = [border[axis_dir % 3 + 3]..=value, value..=border[axis_dir % 3]];
            let s_ranges = [border[axis_dir % 3 + 3]..=value-1, value+1..=border[axis_dir % 3]];

            let mut remain = self.clone();
            let r_axis = [&mut remain.x, &mut remain.y, &mut remain.z];
            *r_axis[axis_dir % 3] = r_ranges[axis_dir / 3].clone();

            let mut split_off = self.clone();
            let s_axis = [&mut split_off.x, &mut split_off.y, &mut split_off.z];
            *s_axis[axis_dir % 3] = s_ranges[1 - (axis_dir / 3)].clone();

            if remain.x.start() > remain.x.end() || remain.y.start() > remain.y.end() || remain.z.start() > remain.z.end()
                || split_off.x.start() > split_off.x.end() || split_off.y.start() > split_off.y.end() || split_off.z.start() > split_off.z.end()
            {
                panic!("Invalid split")
            }

            (remain, Some(split_off))
        } else {
            (self.clone(), None)
        }
    }

    fn contains(&self, (x, y, z): (i32, i32, i32)) -> bool {
        *self.x.start() <= x && x <= *self.x.end()
            && *self.y.start() <= y && y <= *self.y.end()
            && *self.z.start() <= z && z <= *self.z.end()
    }

    fn contains_line(&self, p1: (i32, i32, i32), p2: (i32, i32, i32)) -> bool {
        if p1.0 == p2.0 && p1.1 == p2.1 {
            *self.x.start() <= p1.0 && p1.0 <= *self.x.end()
            && *self.y.start() <= p1.1 && p1.1 <= *self.y.end()
            && (
                (p1.2.min(p2.2)..=p1.2.max(p2.2)).contains(self.z.start())
                || (p1.2.min(p2.2)..=p1.2.max(p2.2)).contains(self.z.end())
            )
        } else if p1.0 == p2.0 && p1.2 == p2.2 {
            *self.x.start() <= p1.0 && p1.0 <= *self.x.end()
                && *self.z.start() <= p1.2 && p1.2 <= *self.z.end()
                && (
                (p1.1.min(p2.1)..=p1.1.max(p2.1)).contains(self.y.start())
                    || (p1.1.min(p2.1)..=p1.1.max(p2.1)).contains(self.y.end())
            )
        } else if p1.2 == p2.2 && p1.1 == p2.1 {
            *self.z.start() <= p1.2 && p1.2 <= *self.z.end()
                && *self.y.start() <= p1.1 && p1.1 <= *self.y.end()
                && (
                (p1.0.min(p2.0)..=p1.0.max(p2.0)).contains(self.x.start())
                    || (p1.0.min(p2.0)..=p1.0.max(p2.0)).contains(self.x.end())
            )
        } else {
            panic!("Invalid line");
        }
    }

    fn clamp_point(&self, (x, y, z): (i32, i32, i32)) -> (i32, i32, i32) {
        (
            x.clamp(*self.x.start(), *self.x.end()),
            y.clamp(*self.y.start(), *self.y.end()),
            z.clamp(*self.z.start(), *self.z.end()),
        )
    }

    fn corners(&self) -> [(i32, i32, i32); 8] {
        [
            (*self.x.start(), *self.y.start(), *self.z.start()),    // 0 ---
            (*self.x.start(), *self.y.start(), *self.z.end()),      // 1 --+
            (*self.x.start(), *self.y.end(), *self.z.start()),      // 2 -+-
            (*self.x.start(), *self.y.end(), *self.z.end()),        // 3 -++
            (*self.x.end(), *self.y.start(), *self.z.start()),      // 4 +--
            (*self.x.end(), *self.y.start(), *self.z.end()),        // 5 +-+
            (*self.x.end(), *self.y.end(), *self.z.start()),        // 6 ++-
            (*self.x.end(), *self.y.end(), *self.z.end()),          // 7 +++
        ]
    }

    fn edges_indices() -> [(usize, usize); 12] {
        [
            (0, 1), (1, 3), (3, 2), (2, 0), // x = -, plane edges
            (0, 4), (1, 5), (3, 7), (2, 6), // x axis lines edges
            (4, 5), (5, 7), (7, 6), (6, 4), // x = +, plane edges
        ]
    }

    fn opposite_corner(corner: usize) -> usize {
        match corner {
            0 => 7, 7 => 0,
            1 => 6, 6 => 1,
            2 => 5, 5 => 2,
            3 => 4, 4 => 3,
            _ => panic!("Invalid corner: {}", corner),
        }
    }
}

#[derive(Debug)]
struct Reactor {
    on_regions: Vec<Region>,
}

impl Reactor {
    pub fn new() -> Reactor {
        Reactor {
            on_regions: vec![]
        }
    }

    pub fn perform_action(&mut self, action: &Action) {
        let mut remaining_action = vec![action.region.clone()];

        if action.action {
            loop {
                let mut done_work = false;

                for i in 0..self.on_regions.len() {
                    remaining_action = remaining_action.into_iter().flat_map(|action_r| {
                        let (mut self_split, intersection, action_split) = self.on_regions[i].intersect(&action_r);

                        if intersection.is_some() {
                            done_work = true;
                        }

                        if !self_split.is_empty() {
                            self.on_regions[i] = self_split.swap_remove(0);
                            self.on_regions.extend(self_split);
                            self.on_regions.extend(intersection);
                        } else if let Some(intersection) = intersection {
                            self.on_regions[i] = intersection;
                        } else {
                            panic!()
                        }

                        action_split
                    }).collect();
                }

                if !done_work {
                    break;
                }
            }
            self.on_regions.extend(remaining_action);
        } else {
            loop {
                let mut done_work = false;

                let mut to_remove = vec![];
                for i in 0..self.on_regions.len() {
                    remaining_action = remaining_action.into_iter().flat_map(|action_r| {
                        let (mut self_split, intersection, action_split) = self.on_regions[i].intersect(&action_r);

                        assert_eq!(self.on_regions[i].volume(), self_split.iter().map(Region::volume).sum::<u128>() + intersection.as_ref().map_or(0, |i| i.volume()));
                        assert_eq!(action_r.volume(), action_split.iter().map(Region::volume).sum::<u128>() + intersection.as_ref().map_or(0, |i| i.volume()));
                        // intersection.as_ref().map_or(0, |i| dbg!(i.volume()));

                        if !self_split.is_empty() {
                            if self_split.len() > 1 {
                                done_work = true;
                            }

                            self.on_regions[i] = self_split.swap_remove(0);
                            self.on_regions.extend(self_split);
                        } else if let Some(_) = intersection {
                            done_work = true;
                            to_remove.push(i);
                        } else {
                            panic!()
                        }

                        action_split
                    }).collect();
                }
                for i in to_remove.into_iter().rev() {
                    self.on_regions.swap_remove(i);
                }

                if !done_work {
                    break;
                }
            }
        }
    }

    pub fn count_on(&self) -> u128 {
        self.on_regions.iter().map(Region::volume).sum()
    }

    pub fn count_on_fully_inside(&self, region: &Region) -> u128 {
        self.on_regions.iter().filter(|r| r.corners().into_iter().all(|c| region.contains(c)))
            .map(Region::volume).sum()
    }
}
