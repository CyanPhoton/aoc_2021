use std::collections::{HashMap, VecDeque};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::num::NonZeroU32;
use std::ops::AddAssign;

fn main() {
    problem_1();
    problem_2();
}

fn for_neighbours<F: FnMut(usize, usize)>(rows: usize, columns: usize, r: usize, c: usize, mut f: F) {
    if r > 0 {
        f(r - 1, c);
    }
    if r < (rows - 1) {
        f(r + 1, c);
    }
    if c > 0 {
        f(r, c - 1);
    }
    if c < (columns - 1) {
        f(r, c + 1);
    }
}

fn problem_1() {
    let reader = BufReader::new(File::open("day9/input.txt").unwrap());

    let height_map: Vec<_> = reader.lines().map(|l| {
        let l = l.unwrap();
        l.chars().map(|c| c.to_digit(10).unwrap()).collect::<Vec<_>>()
    }).collect();

    let rows = height_map.len();
    let columns = height_map[0].len();

    let mut risk_sum = 0;

    for r in 0..rows {
        for c in 0..columns {
            let h = height_map[r][c];
            let mut is_low = true;
            for_neighbours(rows, columns, r, c, |r, c| is_low = is_low && height_map[r][c] > h);

            if is_low {
                risk_sum += h + 1;
            }
        }
    }

    println!("P1: {}", risk_sum);
}

fn problem_2() {
    let reader = BufReader::new(File::open("day9/input.txt").unwrap());

    let mut height_map: Vec<Vec<(u32, Option<NonZeroU32>)>> = reader.lines().map(|l| {
        let l = l.unwrap();
        l.chars().map(|c| (c.to_digit(10).unwrap(), None)).collect::<Vec<_>>()
    }).collect();

    let rows = height_map.len();
    let columns = height_map[0].len();

    let mut next_basin_id = 1;

    for r in 0..rows {
        for c in 0..columns {
            if height_map[r][c].0 < 9 && height_map[r][c].1.is_none() {
                let id = NonZeroU32::new(next_basin_id).unwrap();
                height_map[r][c].1 = Some(id);

                let mut queue = VecDeque::new();
                queue.push_back((r, c));

                while let Some((nr, nc)) = queue.pop_front() {
                    for_neighbours(rows, columns, nr, nc, |ar, ac| {
                        if height_map[ar][ac].1.is_none() && height_map[ar][ac].0 < 9 {
                            height_map[ar][ac].1 = Some(id);
                            queue.push_back((ar, ac));
                        }
                    });
                }

                next_basin_id += 1;
            }
        }
    }

    let mut basin_sizes: HashMap<_, usize> = HashMap::new();

    for r in 0..rows {
        for c in 0..columns {
            if let Some(id) = height_map[r][c].1 {
                basin_sizes.entry(id).or_default().add_assign(1);
            }
        }
    }

    let mut sizes: Vec<_> = basin_sizes.values().copied().collect();
    sizes.sort_unstable();

    let top_three_product = sizes.iter().rev().copied().take(3).product::<usize>();

    println!("P2: {}", top_three_product);
}
