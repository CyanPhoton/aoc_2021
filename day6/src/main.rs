use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    problem_1();
    problem_2();
}

const RESET_STAGE: usize = 6;
const INITIAL_STAGE: usize = 8;
const ADULT_STAGES: usize = RESET_STAGE + 1; // 7
const MATURE_STAGES: usize = INITIAL_STAGE - RESET_STAGE; // 2

#[derive(Default, Debug)]
struct State {
    // Number of fish in each stage, adult stages 0..=6
    adult_cycle: [usize; ADULT_STAGES],
    // Points to stage 0, (ptr + 1) % len is stage 1, etc..
    adult_cycle_ptr: usize,
    // for stages 7, 8
    mature_path: [usize; MATURE_STAGES],
    // points to stage 7
    mature_path_ptr: usize
}

impl State {
    fn step(&mut self) {
        let to_be_born = self.adult_cycle[self.adult_cycle_ptr];
        self.adult_cycle[self.adult_cycle_ptr] = self.adult_cycle[self.adult_cycle_ptr].checked_add(self.mature_path[self.mature_path_ptr])
            .expect("Would overflow");

        self.mature_path[self.mature_path_ptr] = to_be_born;
        self.mature_path_ptr += 1;
        self.mature_path_ptr %= 2;

        self.adult_cycle_ptr += 1;
        self.adult_cycle_ptr %= ADULT_STAGES;
    }

    fn sum(&self) -> usize {
        self.adult_cycle.iter().copied().sum::<usize>()
        + self.mature_path.iter().copied().sum::<usize>()
    }
}

fn problem_1() {
    let reader = BufReader::new(File::open("day6/input.txt").unwrap());
    let line = reader.lines().next().unwrap().unwrap();

    let mut state = State::default();

    for start_stage in line.split(',') {
        let start_stage = start_stage.parse::<usize>().unwrap();
        state.adult_cycle[start_stage] += 1;
    }

    for _ in 0..80 {
        state.step();
    }

    println!("P1: Sum: {}", state.sum());
}

fn problem_2() {
    let reader = BufReader::new(File::open("day6/input.txt").unwrap());
    let line = reader.lines().next().unwrap().unwrap();

    let mut state = State::default();

    for start_stage in line.split(',') {
        let start_stage = start_stage.parse::<usize>().unwrap();
        state.adult_cycle[start_stage] += 1;
    }

    for _ in 0..256 {
        state.step();
    }

    println!("P1: Sum: {}", state.sum());
}
