use std::iter::FusedIterator;
use std::mem::MaybeUninit;
use std::ops::{BitAnd, BitOrAssign, Shl, ShlAssign, ShrAssign};
use num_traits::{Num, NumCast};
use smallvec::SmallVec;

pub trait NextN: Iterator {
    /// Repeatedly calls next upto N times, and if the all return Some, store in array and return,
    /// if less than N where available, return None and all that where acquired are dropped.
    fn next_n<const N: usize>(&mut self) -> Option<[<Self as Iterator>::Item; N]> {
        let mut result = [0; N].map(|_| MaybeUninit::uninit());

        for i in 0..N {
            if let Some(next) = self.next() {
                result[i].write(next);
            } else {
                if std::mem::needs_drop::<<Self as Iterator>::Item>() {
                    // Need to cleanup those that have been acquired in case of Item impl Drop
                    // Since getting index i failed, only indices before i, so [0, i) or 0..i need to be dropped
                    for j in 0..i {
                        unsafe { std::ptr::drop_in_place(result[j].as_mut_ptr()); }
                    }
                }

                return None;
            }
        }

        // Safe since N iterations of loop must have run, so all must be initialised
        let result = result.map(|result| unsafe { result.assume_init() });

        Some(result)
    }
}

pub trait CollectN: Iterator + Sized {
    /// Consumes self to call next_n
    fn collect_n<const N: usize>(mut self) -> Option<[<Self as Iterator>::Item; N]> {
        self.next_n()
    }
}

impl<I: Iterator> NextN for I {}
impl<I: Iterator> CollectN for I {}

#[derive(Clone)]
pub struct CombinationIter<T: Copy, const N: usize> {
    values: SmallVec<[T; 8]>,
    ptr_vec: [usize; N],
    finished: bool
}

impl<T: Copy, const N: usize> Iterator for CombinationIter<T, N> {
    type Item = [T; N];

    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            return None;
        }
        let value = self.ptr_vec.map(|i| self.values[i]);

        let mut ptr_ptr = 0;
        loop {
            if ptr_ptr == N {
                self.finished = true;
                break;
            }
            self.ptr_vec[ptr_ptr] += 1;
            if self.ptr_vec[ptr_ptr] == self.values.len() {
                self.ptr_vec[ptr_ptr] = 0;
                ptr_ptr += 1;
            } else {
                break;
            }
        }

        Some(value)
    }
}

impl<T: Copy, const N: usize> FusedIterator for CombinationIter<T, N> {}

pub trait Combinations: Sized + Iterator where Self::Item: Copy {
    fn combinations<const N: usize>(self) -> CombinationIter<Self::Item, N> {
        CombinationIter {
            values: self.collect(),
            ptr_vec: [0; N],
            finished: false
        }
    }
}

impl<I: Iterator> Combinations for I where I::Item: Copy {}

#[derive(Copy, Clone)]
pub struct BitIterator<T: Num + BitAnd + ShrAssign + Copy> {
    value: T,
    next_lower_pos: u8,
    last_upper_pos: u8,
}

impl<T: Num + BitAnd + ShrAssign + Copy> BitIterator<T> {
    pub fn new(value: T) -> Self {
        BitIterator {
            value,
            next_lower_pos: 0,
            last_upper_pos: (std::mem::size_of::<T>() * 8) as u8
        }
    }
}

impl<T: Num + BitAnd<T, Output=T> + ShrAssign + Copy> Iterator for BitIterator<T> {
    type Item = u8;

    fn next(&mut self) -> Option<u8> {
        if self.next_lower_pos < self.last_upper_pos {
            self.next_lower_pos += 1;
            let b = if self.value & T::one() == T::one() { 1 } else { 0 };
            self.value >>= T::one();
            Some(b)
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = (self.last_upper_pos - self.next_lower_pos) as usize;

        (size, Some(size))
    }
}

impl<T: Num + NumCast + BitAnd<T, Output=T> + ShrAssign + Shl<T, Output=T> + Copy> DoubleEndedIterator for BitIterator<T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.next_lower_pos < self.last_upper_pos {
            self.last_upper_pos -= 1;
            let db = T::one() << T::from(self.last_upper_pos - self.next_lower_pos).unwrap();
            let b = if self.value & db == db { 1 } else { 0 };
            Some(b)
        } else {
            None
        }
    }
}

impl<T: Num + BitAnd<T, Output=T> + ShrAssign + Copy> FusedIterator for BitIterator<T>  {}
impl<T: Num + BitAnd<T, Output=T> + ShrAssign + Copy> ExactSizeIterator for BitIterator<T> {}

pub fn bits_to_int<T: Num + NumCast + ShlAssign + BitOrAssign, const N: usize>(bits: [u8; N]) -> T {
    let mut v = T::zero();
    for bit in bits {
        v <<= T::one();
        v |= T::from(bit).unwrap();
    }
    v
}

pub struct LimitIter<'iter, I: Iterator + ?Sized> {
    iter: &'iter mut I,
    remaining: usize,
}

impl<'iter, I: Iterator + ?Sized> LimitIter<'iter, I> {
    pub fn new(iter: &'iter mut I, limit: usize) -> Self {
        LimitIter {
            iter,
            remaining: limit
        }
    }
}

impl<'iter, I: Iterator> Iterator for LimitIter<'iter, I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        if self.remaining > 0 {
            self.remaining -= 1;
            self.iter.next()
        } else {
            None
        }
    }
}

pub mod utility {
    /// Replaces the existing value, while allowing you to consume the existing value
    pub fn replace_with<T: Sized, F: FnOnce(T) -> T>(dst: &mut T, f: F) {
        let old = unsafe { std::ptr::read(dst) };
        let new = f(old);
        unsafe { std::ptr::write(dst, new); }
    }
}
